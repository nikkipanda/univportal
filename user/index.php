<?php

    include_once '../partials/headers/header.php';

?>

<div class="d-flex justify-content-center align-items-center bgHx" style="width: 100vw; height: 100vh;">
    <div class="usrpanel">
        <img src="../images/icons/university.png" class="img-fluid mx-auto usrlogo">
        <form action="get/usrget.php" method="post" class="m-2 needs-validation" novalidate>
            <div class="mb-3">
                <label for="usrhandle" class="form-label">Username</label>
                <input type="text" class="form-control" id="usrhandle" name="usrhandle" required>
                <div class="invalid-feedback">
                    Username cannot be empty.
                </div>
            </div>
            <div class="mb-4">
                <label for="usrpw" class="form-label">Password</label>
                <input type="password" class="form-control" id="usrpw" name="usrpw" required>
                <div class="invalid-feedback">
                    Password cannot be empty.
                </div>
            </div>
            <div class="d-flex justify-content-center m-2">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
            <p class="text-center"><a href="#" class="adm-link">Forgot password</a></p>
        </form>
    </div>
</div>
    
<script>
(function () {

var forms = document.querySelectorAll('.needs-validation');

Array.prototype.slice.call(forms).forEach(function (form) {

    form.addEventListener('submit', function (event) {

        if (!form.checkValidity()) {

            event.preventDefault();
            event.stopPropagation();

        }

        form.classList.add('was-validated')
    }, false);

})
})();
</script>
</body>
</html>