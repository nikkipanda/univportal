<?php

$method = $_SERVER['REQUEST_METHOD'];

$usr = $_POST['usrhandle'] ?? null;
$pw = $_POST['usrpw'] ?? null;

if ($method === 'POST') {

    require_once '../../database.php';

    $get_user = $pdo->prepare('SELECT * FROM logincred WHERE username = :usr AND `password` = :pw');
    $get_user->bindValue(':usr', $usr);
    $get_user->bindValue(':pw', $pw);

    $get_user->execute();
    $fetch_user = $get_user->fetch(PDO::FETCH_ASSOC);

    if (!empty($fetch_user)) {

        session_start();

        $_SESSION['usr_handle'] = $fetch_user['log_id'];
        $_SESSION['usr_fname'] = $fetch_user['first_name'];
        $_SESSION['usr_lname'] = $fetch_user['last_name'];

        if ($fetch_user['role'] === 'student') {

            header('Location: ../../public/user/student.php');

        } else if ($fetch_user['role'] === 'lecturer') {

            
            
            header('Location: ../../public/user/lecturer.php');

        }

    }
    
    else if (empty($fetch_user)) {

        header('Location: ../index.php');

    }
}
?>