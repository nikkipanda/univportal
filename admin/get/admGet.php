<?php

$reqM = $_SERVER['REQUEST_METHOD'];

$usr = $_POST['usr'] ?? null;
$pw = $_POST['pw'] ?? null;
$dbn = $_POST['dbn'] ?? null;

if ($reqM === 'POST') {

    require_once '../../database.php';

    if ($dbn === 'hedu') {
        $getadm = $pdo->prepare('SELECT * FROM logincred WHERE username = :usr AND `password` = :pw');
        $getadm->bindValue(':usr', $usr);
        $getadm->bindValue(':pw', $pw);

        $getadm->execute();
        $fetchAdm = $getadm->fetch(PDO::FETCH_ASSOC);

        if ($fetchAdm) {

            $accessedDate = date('Y-m-j H:i:s');

            $getadm = $pdo->prepare
            ('INSERT INTO adminlog

                VALUES
                (
                    :id,
                    :dbn,
                    :accessed_date
                )

            ');

            $getadm->bindValue(':id', $fetchAdm['log_id']);
            $getadm->bindValue(':dbn', $dbn);
            $getadm->bindValue(':accessed_date', $accessedDate);

            $getadm->execute();

            session_start();

            $_SESSION = array();

            $_SESSION['userid'] = $fetchAdm['log_id'];
            $_SESSION['adm-fname'] = $fetchAdm['first_name'];
            $_SESSION['adm-lname'] = $fetchAdm['last_name'];

            header('Location: ../../public/index.php');

        } else {

            header('Location: ../index.php');

        }

    } else {

        echo 'NO HIGH SCHOOL DB FOR NOW';

    }

}
?>