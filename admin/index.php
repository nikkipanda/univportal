<?php

    include_once '../partials/headers/header.php';

?>

<div class="d-flex justify-content-center align-items-center pb-5 bgHx" style="width: 100vw; height: 100vh;">
    <div class="d-flex justify-content-center mx-5 ctrDiv">
        <div class="d-flex justify-content-center align-content-center LDiv">
            <form action="get/admGet.php" method="post" class="needs-validation m-3" id="admform" novalidate>
                <div class="form-floating mb-3">
                    <input type="text" class="form-control" id="usr" name="usr" placeholder="username" required>
                    <label for="usr">Username</label>
                    <div class="invalid-feedback">
                        Username required.
                    </div>
                </div>
                <div class="form-floating mb-3">
                    <input type="password" class="form-control" id="pw" name="pw" placeholder="password" required>
                    <label for="pw">Password</label>
                    <div class="invalid-feedback">
                    Password required.
                    </div>
                </div>
                <div class="mb-5">
                    <select class="form-select" name="dbn" required>
                        <option selected disabled value="">Select database</option>
                        <option value="hedu">Higher Education</option>
                        <option value="hsch">High School</option>
                    </select>
                    <div class="invalid-feedback">
                        Please select existing database.
                    </div>
                </div>
                <div class="d-grid gap-2 mb-3">
                    <button class="btn btn-primary" type="submit">Log In</button>
                </div>
                <p class="text-center"><a href="#" class="adm-link">Forgot password</a></p>
            </form>
        </div>
        <div class="justify-content-center align-content-center p-3 RDiv">
            <div class="logowrp">
                <img src="../images/icons/university.png" class="img-fluid mx-auto d-block mb-5 logo">
            </div>
            <p class="lh-base fst-italic text-center prg">"Lorem ipsum dolor sit amet consectetur adipisicing elit. Ipsum assumenda illo laboriosam similique aperiam est veritatis sunt commodi voluptas possimus?"</p>
        </div>
    </div>
</div>

<script>

(function () {

  var forms = document.querySelectorAll('.needs-validation');

  Array.prototype.slice.call(forms).forEach(function (form) {

        form.addEventListener('submit', function (event) {

            if (!form.checkValidity()) {

                event.preventDefault();
                event.stopPropagation();

            }

            form.classList.add('was-validated')
        }, false);

    })
})();

</script>
</body>
</html>
