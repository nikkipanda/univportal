<form action="subject_create.php" method="post" class="needs-validation" id="subject" novalidate>
    <div class="row">
        <div class="col-sm-6 p-3">
            <h3 class="p-2" style="background-color: #F4F4F4;">Term</h3>
            <select class="form-select" name="term" required>
                <option selected disabled value="">Select one</option>
                <option value="Term 1">Term 1</option>
                <option value="Term 2">Term 2</option>
                <option value="Term 3">Term 3</option>
            </select>
            <div class="invalid-feedback">
                Please select a term.
            </div>
        </div>
        <div class="col-sm-6 p-3">
            <h3 class="p-2" style="background-color: #F4F4F4;">Lecturer</h3>
            <select class="form-select" name="lecturer" required>
                <option selected disabled value="">Select one</option>
                <option value="3">Ignacio</option>
                <option value="2">Karen</option>
                <option value="1">John</option>
                <option value="4">Stanley</option>
            </select>
            <div class="invalid-feedback">
                Please select the assigned lecturer.
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6 p-3">
            <h3 class="p-2" style="background-color: #F4F4F4;">Subject</h3>
            <select class="form-select" name="subject" required>
                <option selected disabled value="">Select one</option>
                <option value="1">Business Management</option>
                <option value="2">Sociology</option>
                <option value="3">NSTP I</option>
                <option value="4">NSTP II</option>
                <option value="5">Contemporary Writing</option>
                <option value="6">Rizal</option>
                <option value="7">Purposive Communication</option>
                <option value="8">Practicum</option>
                <option value="9">Introduction to Computing</option>
                <option value="10">Quantitative Methods</option>
                <option value="11">Introduction to HCI</option>
                <option value="12">Information Management</option>
                <option value="13">Application Project 1</option>
                <option value="14">Application Project 2</option>
                <option value="15">Fundamentals of DBMS</option>
                <option value="16">Networking 1</option>
                <option value="17">Calculus</option>
                <option value="18">Networking 2</option>
                <option value="19">Data Structures and Algorithm</option>
            </select>
            <div class="invalid-feedback">
                Please select the subject.
            </div>
        </div>
        <div class="col-sm-6 p-3">
            <h3 class="p-2" style="background-color: #F4F4F4;">Year</h3>
            <select class="form-select" name="student_year" required>
                <option selected disabled value="">Select one</option>
                <option value="Freshman">Freshman</option>
                <option value="Sophomore">Sophomore</option>
                <option value="Junior">Junior</option>
            </select>
            <div class="invalid-feedback">
                Please select the student year.
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 p-3">
            <h3 class="p-2" style="background-color: #F4F4F4;">Schedule</h3>
            <div class="row">
                <div class="col-sm-3">
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="sched" id="mon" value="Mon" required>
                        <label class="form-check-label" for="mon">
                            Monday
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="sched" id="tue" value="Tue">
                        <label class="form-check-label" for="tue">
                            Tuesday
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="sched" id="wed" value="Wed">
                        <label class="form-check-label" for="wed">
                            Wednesday
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="sched" id="thu" value="Thu">
                        <label class="form-check-label" for="thu">
                            Thursday
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="sched" id="fri" value="Fri">
                        <label class="form-check-label" for="fri">
                            Friday
                        </label>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="sched" id="sat" value="Sat">
                        <label class="form-check-label" for="sat">
                            Saturday
                        </label>
                    </div>
                </div>
                <div class="col-sm-6 p-3">
                    <select class="form-select" name="time" required>
                        <option selected disabled value="">Select one</option>
                        <option value="8:00 am">8:00 am</option>
                        <option value="8:30 am">8:30 am</option>
                        <option value="9:00 am">9:00 am</option>
                        <option value="9:30 am">9:30 am</option>
                        <option value="10:00 am">10:00 am</option>
                        <option value="10:30 am">10:30 am</option>
                        <option value="11:00 am">11:00 am</option>
                        <option value="1:00 pm">1:00 pm</option>
                        <option value="1:30 pm">1:30 pm</option>
                        <option value="2:00 pm">2:00 pm</option>
                        <option value="2:30 pm">2:30 pm</option>
                        <option value="3:00 pm">3:00 pm</option>
                        <option value="3:30 pm">3:30 pm</option>
                        <option value="4:00 pm">4:00 pm</option>
                        <option value="4:30 pm">4:30 pm</option>
                    </select>
                    <div class="invalid-feedback">
                        Please select day <b>and</b> time.
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="d-flex justify-content-center col-12 col-sm-12 p-3">
        <button type="submit" class="btn btn-outline-primary px-3 mx-auto">Add</button>
    </div>
    </div>
</form>