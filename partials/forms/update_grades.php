<form action="get/grades.php" method="POST" class="needs-validation" id="updateGrade" data-target-id="" novalidate>
    <div class="row m-1">
        <div class="col-sm shadow-sm m-2" style="background-color: #fff;">
            <div class="d-flex justify-content-between align-items-center">
                <div class="fs-5 my-1">Attendance</div>
                <span class="badge bg-primary">5%</span>
            </div>
            <input type="number" min="60" max="100" step="2" class="form-control mb-3" name="attendance" id="attendance" onchange="previewThis(this)" required>
            <div class="invalid-feedback">
                Attendance cannot be empty.
            </div>
        </div>
        <div class="col-sm shadow-sm m-2" style="background-color: #fff;">
            <div class="d-flex justify-content-between align-items-center">
                <div class="fs-5 my-1">Recitation</div>
                <span class="badge bg-primary">5%</span>
            </div>
            <input type="number" min="60" max="100" step="2" class="form-control mb-3" name="recitation" id="recitation" onchange="previewThis(this)" required>
            <div class="invalid-feedback">
                Recitation cannot be empty.
            </div>
        </div>
    </div>
    <div class="row m-1">
        <div class="col-sm shadow-sm m-2" style="background-color: #fff;">
            <div class="d-flex justify-content-between align-items-center">
                <div class="fs-5 my-1">Homework</div>
                <span class="badge bg-primary">10%</span>
            </div>
            <div id="hwBox">
                <input type="number" min="60" max="100" step="2" class="form-control mb-3" name="homework1" id="homework1" onchange="previewThis(this)" required>
                <div class="invalid-feedback">
                    Homework cannot be empty.
                </div>
            </div>
            <div id="homework"></div>
            <div class="text-decoration-underline text-primary mb-2" data-grade-type="Homework" onclick="addFieldValidate(this)">Add homework</div>
        </div>
        <div class="col-sm shadow-sm m-2" style="background-color: #fff;">
            <div class="d-flex justify-content-between align-items-center">
                <div class="fs-5 my-1">Seatwork</div>
                <span class="badge bg-primary">10%</span>
            </div>
            <input type="number" min="60" max="100" step="2" class="form-control mb-3" name="seatwork1" id="seatwork1" onchange="previewThis(this)" required>
            <div class="invalid-feedback">
                Seatwork cannot be empty.
            </div>
            <div id="seatwork"></div>
            <div class="text-decoration-underline text-primary mb-2" data-grade-type="Seatwork" onclick="addFieldValidate(this)">Add seatwork</div>
        </div>
    </div>
    <div class="row m-1">
        <div class="col-sm shadow-sm m-2" style="background-color: #fff;">
            <div class="d-flex justify-content-between align-items-center">
                <div class="fs-5 my-1">Quiz</div>
                <span class="badge bg-primary">20%</span>
            </div>
            <input type="number" min="60" max="100" step="2" class="form-control mb-3" name="quiz1" id="quiz1" onchange="previewThis(this)" required>
            <div class="invalid-feedback">
                Quiz cannot be empty.
            </div>
            <div id="quiz"></div>
            <div class="text-decoration-underline text-primary mb-2" data-grade-type="Quiz" onclick="addFieldValidate(this)">Add quiz</div>
        </div>
        <div class="col-sm shadow-sm m-2" style="background-color: #fff;">
            <div class="d-flex justify-content-between align-items-center">
                <div class="fs-5 my-1">Prelim Exam</div>
                <span class="badge bg-primary">20%</span>
            </div>
            <input type="number" min="60" max="100" step="2" class="form-control mb-3" name="prelim" id="prelim" onchange="previewThis(this)" required>
            <div class="invalid-feedback">
                Prelim exam cannot be empty.
            </div>
        </div>
    </div>
    <div class="row m-1">
        <div class="col-sm shadow-sm m-2" style="background-color: #fff;">
            <div class="d-flex justify-content-between align-items-center">
                <div class="fs-5 my-1">Midterm Exam</div>
                <span class="badge bg-primary">25%</span>
            </div>
            <input type="number" min="60" max="100" step="2" class="form-control mb-3" name="midterm" id="midterm" onchange="previewThis(this)" required>
            <div class="invalid-feedback">
                Midterm exam cannot be empty.
            </div>
        </div>
        <div class="col-sm shadow-sm m-2" style="background-color: #fff;">
            <div class="d-flex justify-content-between align-items-center">
                <div class="fs-5 my-1">Final Exam</div>
                <span class="badge bg-primary">30%</span>
            </div>
            <input type="number" min="60" max="100" step="2" class="form-control mb-3" name="final" id="final" onchange="previewThis(this)" required>
            <div class="invalid-feedback">
                Final exam cannot be empty.
            </div>
        </div>
    </div>
    <div class="table-responsive" style="background-color: #fff;">
        <div class="fs-5 my-4">Review before submitting:</div>
        <table class="table">
            <thead>
                <tr>
                    <th scope="col" style="width: 12.5%">Attendance</th>
                    <th scope="col" style="width: 12.5%">Recitation</th>
                    <th scope="col" style="width: 12.5%">Homework</th>
                    <th scope="col" style="width: 12.5%">Seatwork</th>
                    <th scope="col" style="width: 12.5%">Quiz</th>
                    <th scope="col" style="width: 12.5%">Prelim</th>
                    <th scope="col" style="width: 12.5%">Midterm</th>
                    <th scope="col" style="width: 12.5%">Final</th>
                </tr>
            </thead>
            <tbody id="gradePrev">
                <tr>
                    <td data-target-categ="attendance"></td>
                    <td data-target-categ="recitation"></td>
                    <td data-target-categ="homework1"></td>
                    <td data-target-categ="seatwork1"></td>
                    <td data-target-categ="quiz1"></td>
                    <td data-target-categ="prelim"></td>
                    <td data-target-categ="midterm"></td>
                    <td data-target-categ="final"></td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="input-group" style="background-color: #fff;">
        <hr class="hrdivider w-100">
        <button type="submit" class="btn rounded btn-primary btn-md mx-auto">Submit</button>
    </div>
</form>

<script>

(function () {
  'use strict'

  var forms = document.querySelectorAll('.needs-validation');

  Array.prototype.slice.call(forms)
    .forEach(function (form) {
      form.addEventListener('submit', function (event) {
        if (!form.checkValidity()) {
          event.preventDefault()
          event.stopPropagation()
        }

        form.classList.add('was-validated')
      }, false)
    })
})()

</script>