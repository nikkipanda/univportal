<form action="" method="POST" enctype="multipart/form-data" class="needs-validation" id="dynamicForm" novalidate>
    <div class="container p-3" style="width: 500px;">
        <div id="personal">
            <div class="input-group mb-3">
                <span class="input-group-text"><label for="firstname">First name</label></span>
                <input type="text" class="form-control" pattern="[a-zA-Z\s]+" id="fname" name="fname" value="<?php echo $fname; ?>" required>
                <div class="invalid-feedback">
                    First name required.
                </div>
            </div>
            <div class="input-group mb-3">
                <span class="input-group-text"><label for="lastname">Last name</label></span>
                <input type="text" class="form-control" pattern="[a-zA-Z\s]+" id="lname" name="lname" value="<?php echo $lname; ?>" required>
                <div class="invalid-feedback">
                    Last name required.
                </div>
            </div>
                <!-- for new record -->
            <div class="input-group mb-3">
                <span class="input-group-text">Gender</span>
                <div class="form-control" style="display: flex; align-items: center; flex-flow: row nowrap; justify-content: center;">
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="gender" id="male" value="m" <?php if($gender == 'm'): echo 'checked'; endif; ?> required>
                        <label class="form-check-label" for="male">Male</label>
                    </div>
                    <div class="invalid-feedback">
                        Gender required.
                    </div>
                </div>
                <div class="form-control" style="display: flex; align-items: center; flex-flow: row nowrap; justify-content: center;">
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="gender" id="female" value="f" <?php if($gender == 'f'): echo 'checked'; endif;?>>
                        <label class="form-check-label" for="female">Female</label>
                    </div>
                </div>
            </div>
            <div class="input-group mb-3">
                <span class="input-group-text"><label for="birthdate">Date of birth</label></span>
                <input type="date" class="form-control" id="birthdate" name="bdate" value="<?php echo $bdate; ?>" required>
                <div class="invalid-feedback">
                    Date of birth required.
                </div>
            </div>
            <div class="input-group mb-3">
                <span class="input-group-text"><label for="address">Home address</label></span>
                <textarea class="form-control" id="address" name="address" required><?php echo $address; ?></textarea>
                <div class="invalid-feedback">
                    Home address required.
                </div>
            </div>
        </div>
        <!-- for new record -->
        <div id="acad">
            <div class="input-group mb-3">
                <span class="input-group-text"><label for="studentnumber">Student number</label></span>
                <input type="text" class="form-control" id="studentnumber" pattern="[\d]+" name="studnumber" value="<?php echo $studnumber; ?>" required>
                <div class="invalid-feedback">
                    Student number required.
                </div>
            </div>
            <div class="input-group mb-3">
                <label class="input-group-text" for="course">Course</label>
                <select class="form-select" id="course" name="course" required>
                    <option selected disabled value="">Select option</option>
                    <option value="BSIT" <?php if($course == 'BSIT'): echo 'selected'; endif; ?>>BSIT</option>
                    <option value="BSCS" <?php if($course == 'BSCS'): echo 'selected'; endif; ?>>BSCS</option>
                    <option value="BSCE" <?php if($course == 'BSCE'): echo 'selected'; endif; ?>>BSCE</option>
                    <option value="BSBA" <?php if($course == 'BSBA'): echo 'selected'; endif; ?>>BSBA</option>
                </select>
                <div class="invalid-feedback">
                    Please select course.
                </div>
            </div>
            <div class="input-group mb-3">
                <label class="input-group-text" for="studentyear">Student year</label>
                <select class="form-select" id="studentyear" name="studyear" required>
                    <option selected disabled value="">Select option</option>
                    <option value="Freshman" <?php if($studyear == 'Freshman'): echo 'selected'; endif; ?>>Freshman</option>
                    <option value="Sophomore" <?php if($studyear == 'Sophomore'): echo 'selected'; endif; ?>>Sophomore</option>
                    <option value="Junior" <?php if($studyear == 'Junior'): echo 'selected'; endif; ?>>Junior</option>
                </select>
                <label class="input-group-text" for="semester">Semester</label>
                <select class="form-select" id="semester" name="sem" required>
                    <option selected disabled value="">Select option</option>
                    <option value="1" <?php if($sem == '1'): echo 'selected'; endif; ?>>1</option>
                    <option value="2" <?php if($sem == '2'): echo 'selected'; endif; ?>>2</option>
                    <option value="3" <?php if($sem == '3'): echo 'selected'; endif; ?>>3</option>
                </select>
                <div class="invalid-feedback">
                    Please select student year and term.
                </div>
            </div>
            <!-- <div class="input-group mb-3">
                <label class="input-group-text" for="studentSection">Section</label>
                <select class="form-select" id="studentSection" name="student_section" required>
                    <option selected disabled value="">Select option</option>
                    <option value="1" >1</option>
                    <option value="2" >2</option>
                    <option value="3" >3</option>
                </select>
                <div class="invalid-feedback">
                    Please select the student's section.
                </div>
            </div> -->
        </div>
        <div id="contactinfo">
            <div class="input-group mb-3">
                <span class="input-group-text"><label for="mobilenum">Mobile number</label></span>
                <input type="text" class="form-control" pattern="[\d]{11}" id="mobilenum" name="mobile" value="<?php echo $mobile; ?>" required>
                <div class="invalid-feedback">
                    Mobile number required.
                </div>
            </div>
            <div class="input-group mb-3">
                <span class="input-group-text"><label for="emailaddress">Email address</label></span>
                <input type="email" class="form-control" id="emailaddress" name="email" value="<?php echo $email; ?>" required>
                <div class="invalid-feedback">
                    Email address required.
                </div>
            </div>
        </div>
        <hr class="hrdivider">
        <div class="input-group mb-3">
            <button type="submit" class="btn btn-primary btn-md mx-auto" id="submitBtn">Submit</button>
        </div>
    </div>
</form>

<script>

(function () {

var forms = document.querySelectorAll('.needs-validation');

Array.prototype.slice.call(forms).forEach(function (form) {

      form.addEventListener('submit', function (event) {

          if (!form.checkValidity()) {

              event.preventDefault();
              event.stopPropagation();

          }

          form.classList.add('was-validated')
      }, false);

  })
})();

</script>