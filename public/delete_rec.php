<?php

    session_start();

    if (!$_SESSION['userid']) {
        header('Location: ../admin/index.php');
    }
    
?>

<?php

require_once '../database.php';

$method = $_SERVER['REQUEST_METHOD'];

$postID = $_POST['deleteID'] ?? null;

$deleteRec = $pdo->prepare('SELECT * FROM students WHERE student_num = :postID');
$deleteRec->bindValue(':postID', $postID);

$deleteRec->execute();
$deleteFetch = $deleteRec->fetch(PDO::FETCH_ASSOC);

if ($method === 'POST') {

    $deleteRec = $pdo->prepare('DELETE FROM students WHERE student_num = :postID');
    $deleteRec->bindValue(':postID', $postID);

    $deleteRec->execute();

    header('Location: index.php');
}
?>
</body>
</html>