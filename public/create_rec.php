<?php

    session_start();

    if (!$_SESSION['userid']) {
        header('Location: ../admin/index.php');
    }
    
?>

<?php

require_once '../database.php';

$statement = $pdo->prepare('SELECT * FROM students');

$statement->execute();
$studentFetch = $statement->fetchAll(PDO::FETCH_ASSOC);

$method = $_SERVER['REQUEST_METHOD'];

$warningMsg = [];
$fname = '';
$lname = '';
$gender = '';
$studnumber = '';
$bdate = '';
$course = '';
$studyear = '';
$sem = '';
$student_section = '';
$address = '';
$mobile = '';
$email = '';
$date_created = date('Y-m-d H:i:s');

if ($method === 'POST'):

    // push warnings
    require_once '../validate.php';

    if (empty($warningMsg) && $warningMsg !== null):
        
        $statement = $pdo->prepare
        ("INSERT INTO students
            (
                student_num,
                last_name,
                first_name,
                birthdate,
                home_address,
                course,
                student_year,
                school_term,
                section,
                mobile_num,
                email_add,
                sex,
                date_created
            )

            VALUES (
                :studentID,
                :lastName,
                :firstName,
                :birthdate,
                :homeAddress,
                :course,
                :studentYear,
                :schoolTerm,
                :student_section,
                :mobileNum,
                :emailAdd,
                :sex,
                :date_created
            )
        ");

        $statement->bindValue(':studentID', $studnumber);
        $statement->bindValue(':lastName', $lname);
        $statement->bindValue(':firstName', $fname);
        $statement->bindValue(':birthdate', $bdate);
        $statement->bindValue(':homeAddress', $address);
        $statement->bindValue(':course', $course);
        $statement->bindValue(':studentYear', $studyear);
        $statement->bindValue(':schoolTerm', $sem);
        $statement->bindValue(':student_section', $student_section);
        $statement->bindValue(':mobileNum', $mobile);
        $statement->bindValue(':emailAdd', $email);
        $statement->bindValue(':sex', $gender);
        $statement->bindValue(':date_created', $date_created);

        $statement->execute();

        header('Location: index.php');

    endif;
endif;

?>

<?php

    include_once '../partials/headers/header.php';

?>

<!-- <div class="container p-5" style="background-color: indianred;">


</div> -->
<nav class="navbar sticky-top navbar-expand-lg navbar-light" style="background-color: rgb(234, 251, 255);">
    <div class="container-fluid my-3">
        <a class="navbar-brand" href="#">Brand logo/name</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#toggleNav" aria-expanded="false">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="toggleNav">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                <li class="nav-item">
                    <a class="nav-link" aria-current="page" href="index.php">Home</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="toggleAxn" role="button" data-bs-toggle="dropdown" aria-expanded="false">Action</a>
                    <ul class="dropdown-menu" aria-labelledby="toggleAxn">
                        <li><a class="dropdown-item" href="create_rec.php">Create new record</a></li>
                        <li><hr class="dropdown-divider"></li>
                        <li><a class="dropdown-item" href="#">Dump</a></li>
                    </ul>
                </li>
                <li class="nav-item mx-2">
                    <a class="nav-link" aria-current="page" href="subjects.php">Subjects</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="../admin/get/logout.php">Log Out</a>
                </li>
            </ul>
            <ul class="nav nav-pills">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" data-bs-toggle="dropdown" href="#" role="button" aria-expanded="false">Hi, <?php echo $_SESSION['adm-fname']; ?></a>
                    <ul class="dropdown-menu">
                    <li><a class="dropdown-item" href="#">Profile</a></li>
                    <li><a class="dropdown-item" href="#">Messages</a></li>
                    <li><a class="dropdown-item" href="#">Settings</a></li>
                    <li><a class="dropdown-item" href="#">Dark mode</a></li>
                    <li><hr class="dropdown-divider"></li>
                    <li><a class="dropdown-item" href="../admin/get/logout.php">Sign out</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>
<div class="container-fluid my-4">
    <h3 class="my-4"> Create new record </h3>
    <a class="btn btn-primary" href="index.php">Go back</a>
    <div class="d-flex crtform" id="crtform">
        <!-- form -->
        <?php
            include_once '../partials/forms/studentForm.php';
        ?>
        <!-- <div id="prsnldv" style="background-color: indianred;">
            <div id="lf"></div>
        </div>
        <div id="cddv" style="background-color: indianred;"></div>
        <div id="cntctnfdv" style="background-color: indianred;"></div>
        <div id="sbmtdv" style="background-color: indianred;"></div> -->
    </div>
</div>

<script>

// var crtform = document.getElementById('crtform');
// var personal = document.getElementById('personal');
// var acad = document.getElementById('acad');
// var contactinfo = document.getElementById('contactinfo');
// var sbmt = document.getElementById('sbmt');
// var prsnldv = document.getElementById('prsnldv');
// var cddv = document.getElementById('cddv');
// var cntctnfdv = document.getElementById('cntctnfdv');
// var sbmtdv = document.getElementById('sbmtdv');

// console.log('personal: ', prsnldv);
// console.log('acad: ', cddv);
// console.log('contact: ', cntctnfdv);

</script>
</body>
</html>