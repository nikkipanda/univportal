<?php

require_once '../database.php';

$method = $_SERVER['REQUEST_METHOD'];

$subject = $_POST['subject'] ?? null;
$lecturer = $_POST['lecturer'] ?? null;
$student_year = $_POST['student_year'] ?? null;
$term = $_POST['term'] ?? null;
$sched_day = $_POST['sched'] ?? null;
$sched_time = $_POST['time'] ?? null;
$date_created = date('Y-m-d H:i:s');
$subject_term = [];

if ($method === 'POST') {

    require '../database.php';

    $lecturer_get = $pdo->prepare('SELECT * FROM lecturers where id = :lecturer_id');
    $lecturer_get->bindValue(':lecturer_id', $lecturer);

    $lecturer_get->execute();
    $lecturer_fetch = $lecturer_get->fetch(PDO::FETCH_ASSOC);

    $subject_get = $pdo->prepare('SELECT * FROM subjects where id = :subject_id');
    $subject_get->bindValue(':subject_id', $subject);

    $subject_get->execute();
    $subject_fetch = $subject_get->fetch(PDO::FETCH_ASSOC);

    if ($subject_fetch && $lecturer_fetch) {

        $add_subject = $pdo->prepare
        ('INSERT INTO subject_term

            (
                subject_id,
                lecturer_id,
                subject_code,
                subject_title,
                unit,
                lecturer_lname,
                lecturer_fname,
                student_year,
                term,
                sched_day,
                sched_time,
                date_created
            )

        VALUES

            (
                :subject_id,
                :lecturer_id,
                :subject_code,
                :subject_title,
                :unit,
                :lecturer_lname,
                :lecturer_fname,
                :student_year,
                :term,
                :sched_day,
                :sched_time,
                :date_created
            )

        ');

        $add_subject->bindValue(':subject_id', $subject_fetch['id']);
        $add_subject->bindValue(':lecturer_id', $lecturer_fetch['id']);
        $add_subject->bindValue(':subject_code', $subject_fetch['subject_code']);
        $add_subject->bindValue(':subject_title', $subject_fetch['subject_title']);
        $add_subject->bindValue(':unit', $subject_fetch['unit']);
        $add_subject->bindValue(':lecturer_lname', $lecturer_fetch['last_name']);
        $add_subject->bindValue(':lecturer_fname', $lecturer_fetch['first_name']);
        $add_subject->bindValue(':student_year', $student_year);
        $add_subject->bindValue(':term', $term);
        $add_subject->bindValue(':sched_day', $sched_day);
        $add_subject->bindValue(':sched_time', $sched_time);
        $add_subject->bindValue(':date_created', $date_created);

        $add_subject->execute();

        session_start();
        
        $_SESSION['subject_added'] = 'true';
        $_SESSION['subject_code'] = $subject_fetch['subject_code'];
        $_SESSION['subject_title'] = $subject_fetch['subject_title'];

        header('Location: ./subjects.php');

    }

    if (!$subject_fetch) {

        echo 'Subject does not exist.';

    }

    if (!$lecturer_fetch) {

        echo 'Lecturer is not registered.';
        
    }

}

?>