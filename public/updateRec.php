<?php

    session_start();

    if (!$_SESSION['userid']) {
        header('Location: ../admin/index.php');
    }
    
?>

<?php

require_once '../database.php';

$getID = $_GET['getID'] ?? null; 

$postID = $_POST['studnumber'] ?? null;

$method = $_SERVER['REQUEST_METHOD'];

$getRec = $pdo->prepare('SELECT * FROM students WHERE student_num = :getID');
$getRec->bindValue(':getID', $getID);

$getRec->execute();
$getStudentRec = $getRec->fetch(PDO::FETCH_ASSOC);

$getStudentArr = json_encode($getStudentRec, JSON_PRETTY_PRINT);

echo $getStudentArr;

$warningMsg = [];
$studnumber = '';
$fname = '';
$lname = '';
$gender = '';
$bdate = '';
$course = '';
$studyear = '';
$sem = '';
$student_section = '';
$address = '';
$mobile = '';
$email = '';
$date_updated = date('Y-m-d H:i:s');

if ($method === 'POST') {

    require_once '../validate.php';

    if (empty($warningMsg) && $warningMsg !== null):
      
      $getRec = $pdo->prepare
      ("UPDATE students SET
      
          last_name = :lastName,
          first_name = :firstName,
          birthdate = :birthdate,
          home_address = :homeAddress,
          course = :course,
          student_year = :studentYear,
          school_term = :schoolTerm,
          section = :student_section,
          mobile_num = :mobileNum,
          email_add = :emailAdd

        WHERE
          student_num = $postID
      ");

      $getRec->bindValue(':lastName', $lname);
      $getRec->bindValue(':firstName', $fname);
      $getRec->bindValue(':birthdate', $bdate);
      $getRec->bindValue(':homeAddress', $address);
      $getRec->bindValue(':course', $course);
      $getRec->bindValue(':studentYear', $studyear);
      $getRec->bindValue(':schoolTerm', $sem);
      $getRec->bindValue(':student_section', $student_section);
      $getRec->bindValue(':mobileNum', $mobile);
      $getRec->bindValue(':emailAdd', $email);

      $getRec->execute();

      header('Location: index.php');

  endif;
}

?>