<?php

require_once '../../database.php';

$method = $_SERVER['REQUEST_METHOD'];

$raw_input = file_get_contents('php://input');
$explode_subject = explode('&', str_replace('"', '', $raw_input));
$checked_subject = [];

$checked_subject['add_drop_id'] = $explode_subject[0] ?? null;
$checked_subject['add_drop_subject'] = $explode_subject[1]  ?? null;

$date_updated = date('Y-m-d H:i:s');

if ($method === 'POST' && $checked_subject['add_drop_id']) {
    
    if ($checked_subject['add_drop_subject']) {
        

        $update_row = $pdo->prepare('SELECT * FROM subject_term WHERE id = :subject_id');
        $update_row->bindValue(':subject_id', $checked_subject['add_drop_subject']);

        $update_row->execute();
        $subject_info_fetch = $update_row->fetch(PDO::FETCH_ASSOC);

        $update_info = $pdo->prepare
        ('INSERT INTO grades
            
            (
                student_id,
                subject_term,
                subject_code,
                subject_title,
                unit,
                student_year,
                term,
                section,
                sched_day,
                sched_time,
                lecturer_lname,
                lecturer_fname,
                date_created
            )

            VALUES
            
            (
                :student_id,
                :subject_term_id,
                :subject_code,
                :subject_title,
                :unit,
                :student_year,
                :term,
                :section,
                :sched_day,
                :sched_time,
                :lecturer_lname,
                :lecturer_fname,
                :date_created
            )
        ');

        $update_info->bindValue(':student_id', $checked_subject['add_drop_id']);
        $update_info->bindValue(':subject_term_id', $subject_info_fetch['id']);
        $update_info->bindValue(':subject_code', $subject_info_fetch['subject_code']);
        $update_info->bindValue(':subject_title', $subject_info_fetch['subject_title']);
        $update_info->bindValue(':unit', $subject_info_fetch['unit']);
        $update_info->bindValue(':student_year', $subject_info_fetch['student_year']);
        $update_info->bindValue(':term', $subject_info_fetch['term']);
        $update_info->bindValue(':section', $subject_info_fetch['section']);
        $update_info->bindValue(':sched_day', $subject_info_fetch['sched_day']);
        $update_info->bindValue(':sched_time', $subject_info_fetch['sched_time']);
        $update_info->bindValue(':lecturer_lname', $subject_info_fetch['lecturer_lname']);
        $update_info->bindValue(':lecturer_fname', $subject_info_fetch['lecturer_fname']);
        $update_info->bindValue(':date_created', $date_updated);

        $update_info->execute();

        $return_row = $pdo->prepare('SELECT * FROM grades WHERE student_id = :student_id AND subject_term = :subject_id');
        $return_row->bindValue(':student_id', $checked_subject['add_drop_id']);
        $return_row->bindValue(':subject_id', $checked_subject['add_drop_subject']);

        $return_row->execute();
        $return_fetch_row = $return_row->fetch(PDO::FETCH_ASSOC);

        $success_alert = 'Added';
        $success_json = json_encode($return_fetch_row, JSON_PRETTY_PRINT);

        echo $success_json;

    } else {

        echo 'The student is not enrolled to this subject.';

    }
}

else if ($method === 'POST' && empty($checked_subject['add_drop_id'])) {

    echo 'Student is not registered';

}

?>