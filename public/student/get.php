<?php

$q = $_GET['q'] ?? null;

$method = $_SERVER['REQUEST_METHOD'];

if (!empty($q) && $method === 'GET' && $q !== '') {

    require '../../database.php';

    $qStr = '%'.$q.'%';

    $statement = $pdo->prepare
    ('SELECT * FROM students

        WHERE student_num LIKE :qStr
        OR last_name LIKE :qStr
        OR first_name LIKE :qStr
        OR birthdate LIKE :qStr
        OR home_address LIKE :qStr
        OR course LIKE :qStr
        OR mobile_num LIKE :qStr
        OR email_add LIKE :qStr
    
    ');

    $statement->bindValue(':qStr', $qStr);

    $statement->execute();
    $studentFetch = $statement->fetchAll(PDO::FETCH_ASSOC);

    $studentInfo = json_encode($studentFetch, JSON_PRETTY_PRINT);

    echo $studentInfo;

} else {

    require '../database.php';

    $statement = $pdo->prepare('SELECT * FROM students ORDER BY last_name ASC');

    $statement->execute();
    $studentFetch = $statement->fetchAll(PDO::FETCH_ASSOC);

    $get_all_subjects = $pdo->prepare('SELECT * FROM subject_term ORDER BY subject_code ASC');

    $get_all_subjects->execute();
    $all_subjects_fetch = $get_all_subjects->fetchAll(PDO::FETCH_ASSOC);

    $studentInfo = json_encode($studentFetch, JSON_PRETTY_PRINT);
    $all_subjects_json = json_encode($all_subjects_fetch, JSON_PRETTY_PRINT);

}

$warningMsg = [];
$fname = '';
$lname = '';
$gender = '';
$studnumber = '';
$bdate = '';
$course = '';
$studyear = '';
$sem = '';
$student_section = '';
$address = '';
$mobile = '';
$email = '';

?>