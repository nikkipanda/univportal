<?php

require_once '../../database.php';

$method = $_SERVER['REQUEST_METHOD'];

$get_id = $_GET['student'] ?? null;
$get_subject = $_GET['subject'] ?? null;
$enrolled = 'enrolled';
$not_enrolled = 'not enrolled';

$enrolled = json_encode($enrolled, JSON_PRETTY_PRINT);
$not_enrolled = json_encode($not_enrolled, JSON_PRETTY_PRINT);

if ($method === 'GET' && $get_id) {

    if ($get_subject) {

        $get_row = $pdo->prepare('SELECT * FROM grades WHERE student_id = :student_id AND subject_term = :subject_id');
        $get_row->bindValue(':student_id', $get_id);
        $get_row->bindValue(':subject_id', $get_subject);

        $get_row->execute();
        $row_fetch = $get_row->fetch(PDO::FETCH_ASSOC);

        if (!empty($row_fetch)) {

            echo $enrolled;

        } else {

            echo $not_enrolled;

        }
        

    } else {

        echo 'Subject is not open this term or does not exist at all.';    

    }

}

else {

    echo 'Student is not registered';
    
}

?>