<?php

require_once '../../database.php';

$method = $_SERVER['REQUEST_METHOD'];

$raw_input = file_get_contents('php://input');
$explode_subject = explode('&', str_replace('"', '', $raw_input));
$checked_subject = [];

$checked_subject['add_drop_id'] = $explode_subject[0] ?? null;
$checked_subject['add_drop_subject'] = $explode_subject[1]  ?? null;

$date_updated = date('Y-m-d H:i:s');

if ($method === 'POST' && $checked_subject['add_drop_id']) {
    
    if ($checked_subject['add_drop_subject']) {
        
        $delete_row = $pdo->prepare('DELETE FROM grades WHERE student_id = :student_id AND subject_term = :subject_id');
        $delete_row->bindValue(':student_id', $checked_subject['add_drop_id']);
        $delete_row->bindValue(':subject_id', $checked_subject['add_drop_subject']);

        $delete_row->execute();

    } else {

        echo 'The student is not enrolled to this subject.';
        
    }

    $success_alert = 'Deleted';
    $success_json = json_encode($success_alert, JSON_PRETTY_PRINT);

    echo $success_json;
}

else if ($method === 'POST' && empty($checked_subject['add_drop_id'])) {

    echo 'Student is not registered';

}

?>