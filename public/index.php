<?php

    include_once '../partials/headers/header.php';
    include_once 'student/get.php';

?>

<?php

    session_start();

    if (!$_SESSION['userid']) {
        header('Location: ../admin/index.php');
    }
    
?>

<nav class="navbar sticky-top navbar-expand-lg navbar-light" style="background-color: rgb(234, 251, 255);">
    <div class="container-fluid">
        <a class="navbar-brand" href="index.php">Brand logo/name</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#toggleNav" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="toggleNav">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                <li class="nav-item mx-2">
                    <a class="nav-link active" aria-current="page" href="index.php">Home</a>
                </li>
                <li class="nav-item dropdown mx-2">
                    <a class="nav-link dropdown-toggle" href="#" id="toggleAxn" role="button" data-bs-toggle="dropdown" aria-expanded="false">Action</a>
                    <ul class="dropdown-menu" aria-labelledby="toggleAxn">
                        <li><a class="dropdown-item" href="create_rec.php">Create new record</a></li>
                        <!-- <li><a class="dropdown-item" href="add_drop_subject.php">Add/drop subject</a></li> -->
                        <li><hr class="dropdown-divider"></li>
                        <li><a class="dropdown-item" href="#">Dump</a></li>
                    </ul>
                </li>
                <li class="nav-item mx-2">
                    <a class="nav-link" aria-current="page" href="subjects.php">Subjects</a>
                </li>
            </ul>
            <ul class="nav nav-pills">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" data-bs-toggle="dropdown" href="#" role="button" aria-expanded="false">Hi, <?php echo $_SESSION['adm-fname']; ?></a>
                    <ul class="dropdown-menu">
                    <li><a class="dropdown-item" href="#">Profile</a></li>
                    <li><a class="dropdown-item" href="#">Messages</a></li>
                    <li><a class="dropdown-item" href="#">Settings</a></li>
                    <li><a class="dropdown-item" href="#">Dark mode</a></li>
                    <li><hr class="dropdown-divider"></li>
                    <li><a class="dropdown-item" href="../admin/get/logout.php">Sign out</a></li>
                    </ul>
                </li>
            </ul>
            <form class="d-flex mt-3" action="student/get.php" id="q">
                <input class="form-control me-2" type="search" name="q" placeholder="Enter keyword" aria-label="Search" id="qry">
                <button class="btn btn-outline-dark" type="submit">Search</button>
            </form>
        </div>
    </div>
</nav>
    <!-- <div class="d-flex bd-highlight usrnav">
        <div class="d-flex p-2 bd-highlight">
            <span class="align-self-center">
        </div>
        <div class="ms-auto p-2 bd-highlight">
            <a class="nav-link tglnk" href="../admin/get/logout.php">Sign Out</a>
            <img src="../images/icons/logout.png" class="res-icon">
        </div>
    </div> -->
    <div class="container-fluid my-4">
    <div class="d-flex justify-content-between align-items-center m-3 max-vw">
        <div id="studentTotal" class="min-vw"></div>
        <div class="input-group">
            <select class="form-select" id="recordsPerPage" onchange="changeRecNum(this)">
                <option value="10" selected>10</option>
                <option value="20">20</option>
                <option value="50">50</option>
                <option value="100">100</option>
                <option value="150">150</option>
                <option value="200">200</option>
            </select>
        </div>
    </div>
    <div class="table-responsive mb-3">
        <table class="table table-striped table-bordered table-hover">
            <thead>
                <tr>
                    <th scope="col">Student #</th>
                    <th scope="col" style="width: 14.29%">Last Name</th>
                    <th scope="col" style="width: 14.29%">First Name</th>
                    <!-- <th scope="col" style="width: 11.11%">Birthdate</th> -->
                    <!-- <th scope="col" style="width: 8.33%">Address</th> -->
                    <th scope="col" style="width: 9.29%">Course</th>
                    <th scope="col" style="width: 14.29%">Year</th>
                    <th scope="col">Term</th>
                    <!-- <th scope="col" style="width: 8.33%">Section</th> -->
                    <th scope="col" style="width: 14.29%">Mobile &#x23;</th>
                    <th scope="col" style="width: 14.29%">Email Address</th>
                    <th scope="col" style="width: 19.29%">Action</th>
                </tr>
            </thead>
            <tbody id="tableRec" style="vertical-align: middle;"></tbody>
        </table>
    </div>
</div>
<footer class="container-fluid d-flex justify-content-center mb-5" id="footerID"></footer>
<div class="modal fade" id="updateRec" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Update</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div id="updateBox">
                    <?php
                        include_once '../partials/forms/studentForm.php';
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="addDrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Add/drop subjects</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="m-3" id="subjectTableDesc">No record to show.</div>
                <div class="table-responsive mb-3">
                    <table class="table table-striped table-bordered table-hover">
                        <thead class="align-middle">
                            <tr>
                                <th scope="col" style="width: 14.29%">Subject Code</th>
                                <th scope="col" style="width: 14.29%">Subject Title</th>
                                <th scope="col" style="width: 14.29%">Lecturer name</th>
                                <th scope="col" style="width: 14.29%">Schedule</th>
                                <th scope="col" style="width: 14.29%">Year & Section</th>
                                <th scope="col" style="width: 14.29%">Term</th>
                            </tr>
                        </thead>
                        <tbody id="subjectTable" style="vertical-align: middle;"></tbody>
                    </table>
                </div>
                <div id="subjectHeader"></div>
                <?php
                    include_once '../partials/forms/add_drop_subject.php';
                ?>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="deleteRec" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Confirm deletion</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div id="confirmDel"></div>
                <form action="delete_rec.php" method="POST" id="delForm">
                    <hr class="hrdivider">
                    <div class="input-group mb-3">
                        <input type="hidden" name="deleteID" id="deleteID">
                        <button type="submit" class="btn rounded btn-danger btn-md mx-auto">Delete</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>

var footerID = document.getElementById('footerID');
var getInfo = <?php echo $studentInfo; ?>;
var allSubjects = <?php echo $all_subjects_json; ?>;
var tableRec = document.getElementById('tableRec');
var getVal = document.getElementById('recordsPerPage').value;
var studentTotal = document.getElementById('studentTotal');
var recordChunk = Math.ceil(getInfo.length / getVal);
// var addDrop = document.getElementById('addDrop');
var addDropForm = document.getElementById('addDropForm');
var subjectTableDesc = document.getElementById('subjectTableDesc');
var subjectsBox = document.getElementById('subjectsBox');
var subjectHeader = document.getElementById('subjectHeader');
// var addDropHR = document.getElementById('addDropHR');
var addDropBox = document.getElementById('addDropBox');
// var addDropStudentID = document.getElementById('addDropStudentID');
var subjectTable = document.getElementById('subjectTable');
var q = document.getElementById('q');
var chunk = {};
var z = 0;

studentTotal.innerHTML = 'Number of registered students: ' + getInfo.length;

q.addEventListener('submit', search);

function search(event) {

    event.preventDefault();

    var qry = document.getElementById('qry');

    if (!(qry.value) == '' || !(qry.value) == null) {

        var newQ = new XMLHttpRequest;

        newQ.open('GET', this.getAttribute('action') + '?q=' + qry.value, true);

        newQ.onprogress = function() {
            console.log('please wait');
        }

        newQ.onload = function() {

            tableRec.innerHTML = '';

            getInfo = JSON.parse(this.responseText);

            chunk = getInfo.slice(0, getVal);
            genBtn(getVal);
            changePage();

        }

        newQ.send();

    } else {

        location.reload();

    }
}

chunk = getInfo.slice(0, getVal);
genBtn(getVal);
changePage();

function changeRecNum(e) {

    tableRec.innerHTML = '';
    chunk = getInfo.slice(0, e.value);

    genBtn(e.value);
    changePage();

    // removed param for changePage (getInfo) @ 04/05/2021
    
}

function genBtn(selectOpt) {

    footerID.innerHTML = '';
    
    recordChunk = Math.ceil(getInfo.length / selectOpt);

    if (getInfo.length > selectOpt) {

        for (var i = 1; i <= recordChunk; i++) {
            
            var newBtn = document.createElement('button');
            newBtn.className = 'btn btn-primary mx-2';
            newBtn.setAttribute('id', i);
            newBtn.setAttribute('onclick', 'showRec(this)');
            newBtn.dataset.selectOpt = selectOpt;
            newBtn.innerHTML = i;
            footerID.append(newBtn);

        }
    }
}

function changePage() {

    for (z in chunk) {

        var newRow = document.createElement('tr');
        var newID = document.createElement('td');
        var newLName = document.createElement('td');
        var newFName = document.createElement('td');
        // var newBirthdate = document.createElement('td');
        var newHomeAddress = document.createElement('td');
        var newCourse  = document.createElement('td');
        var newStudentYear = document.createElement('td');
        var newSchoolTerm = document.createElement('td');
        // var newSection = document.createElement('td');
        var newMobileNum = document.createElement('td');
        var newEmailAdd = document.createElement('td');
        var newAction = document.createElement('td');
        var newSubjectBtn = document.createElement('button');
        var newSubjectIcon = document.createElement('img');
        var newUpdateBtn = document.createElement('button');
        var newUpdIcon = document.createElement('img');
        var newDelBtn = document.createElement('button');
        var newDelIcon = document.createElement('img');

        newID.style.fontWeight = '600';

        newSubjectBtn.setAttribute('onclick', 'addDropSub(this)');
        newSubjectBtn.setAttribute('type', 'button');
        newSubjectBtn.setAttribute('class', 'btn m-1');
        newSubjectBtn.setAttribute('data-bs-toggle', 'modal');
        newSubjectBtn.setAttribute('data-bs-target', '#addDrop');
        newSubjectBtn.setAttribute('data-add-drop', chunk[z]['id']);
        newSubjectIcon.setAttribute('src', '../images/icons/test.png');
        newSubjectIcon.style.width = '25px';
        newSubjectIcon.style.height = 'auto';
        newSubjectBtn.appendChild(newSubjectIcon);

        newUpdateBtn.setAttribute('onclick', 'viewThis(this)');
        newUpdateBtn.setAttribute('type', 'button');
        newUpdateBtn.setAttribute('class', 'btn m-1');
        newUpdateBtn.setAttribute('data-bs-toggle', 'modal');
        newUpdateBtn.setAttribute('data-bs-target', '#updateRec');
        newUpdateBtn.setAttribute('id', chunk[z]['student_num']);
        newUpdIcon.setAttribute('src', '../images/icons/edit.png');
        newUpdIcon.style.width = '25px';
        newUpdIcon.style.height = 'auto';
        newUpdateBtn.appendChild(newUpdIcon);

        newDelBtn.setAttribute('onclick', 'delThis(this)');
        newDelBtn.setAttribute('type', 'button');
        newDelBtn.setAttribute('class', 'btn m-1');
        newDelBtn.setAttribute('data-bs-toggle', 'modal');
        newDelBtn.setAttribute('data-bs-target', '#deleteRec');
        newDelBtn.setAttribute('data-studentLName', chunk[z]['last_name']);
        newDelBtn.setAttribute('data-studentFName', chunk[z]['first_name']);
        newDelBtn.setAttribute('data-studentCourse', chunk[z]['course']);
        newDelBtn.setAttribute('data-studentBdate', chunk[z]['birthdate']);
        newDelBtn.setAttribute('data-studentYear', chunk[z]['student_year']);
        newDelBtn.setAttribute('data-homeAddress', chunk[z]['home_address']);
        newDelBtn.setAttribute('data-schoolTerm', chunk[z]['school_term']);
        newDelBtn.setAttribute('data-studentSection', chunk[z]['section'])
        newDelBtn.setAttribute('data-studentMobile', chunk[z]['mobile_num']);
        newDelBtn.setAttribute('data-studentEmail', chunk[z]['email_add']);
        newDelBtn.setAttribute('data-deleteID', chunk[z]['student_num']);
        newDelIcon.setAttribute('src', '../images/icons/delete.png');
        newDelIcon.style.width = '25px';
        newDelIcon.style.height = 'auto';
        newDelBtn.appendChild(newDelIcon);

        newID.innerHTML = chunk[z]['student_num'];
        newLName.innerHTML = chunk[z]['last_name'];
        newFName.innerHTML = chunk[z]['first_name'];
        // newBirthdate.innerHTML = chunk[z]['birthdate'];
        // newHomeAddress.innerHTML = chunk[z]['home_address'];
        newCourse.innerHTML = chunk[z]['course'];
        newStudentYear.innerHTML = chunk[z]['student_year'];
        newSchoolTerm.innerHTML = chunk[z]['school_term'];
        // newSection.innerHTML = chunk[z]['section'];
        newMobileNum.innerHTML = chunk[z]['mobile_num'];
        newEmailAdd.innerHTML = chunk[z]['email_add'];

        newRow.appendChild(newID);
        newRow.appendChild(newLName);
        newRow.appendChild(newFName);
        // newRow.appendChild(newBirthdate);
        // newRow.appendChild(newHomeAddress);
        newRow.appendChild(newCourse);
        newRow.appendChild(newStudentYear);
        newRow.appendChild(newSchoolTerm);
        // newRow.appendChild(newSection);
        newRow.appendChild(newMobileNum);
        newRow.appendChild(newEmailAdd);
        newAction.appendChild(newUpdateBtn);
        newAction.appendChild(newSubjectBtn);
        newAction.appendChild(newDelBtn);
        newRow.appendChild(newAction);

        tableRec.appendChild(newRow);

    }

}

function showRec(ID) {

    var selectedVal = ID.dataset.selectOpt;
    tableRec.innerHTML = '';

    if (getInfo.length < (selectedVal * ID.id)) {
        chunk = getInfo.slice((selectedVal * ID.id) - selectedVal);
    } else if (getInfo.length >= (selectedVal * ID.id)) {
        chunk = getInfo.slice((selectedVal * ID.id) - selectedVal, (selectedVal * ID.id));
    }

    changePage();

}

function viewThis(thisID) {

    var submitBtnID = thisID.id;

    var setView = document.getElementById('toUpdate');
    var updateBox = document.getElementById('updateBox');

    var importForm = new XMLHttpRequest();

    importForm.open('GET', 'updateRec.php?getID=' + submitBtnID, true);

    importForm.onprogress = function() {
        console.log('Please wait');
    }

    importForm.onload = function() {

        var viewInfo = JSON.parse(this.responseText);
        
        var fname = document.getElementById('fname');
        var lname = document.getElementById('lname');
        var gender = document.getElementsByName('gender');
        var bdate = document.getElementById('birthdate');
        var studnumber = document.getElementById('studentnumber');
        var address = document.getElementById('address');
        var mobile = document.getElementById('mobilenum');
        var email = document.getElementById('emailaddress');
        var genderM = document.getElementById('male');
        var genderF = document.getElementById('female');
        var courseOpt = document.getElementById('course').options;
        var studentYear = document.getElementById('studentyear').options;
        var semester = document.getElementById('semester').options;
        // var student_section = document.getElementById('studentSection').options;

        var dynamicForm = document.getElementById('dynamicForm');
        var submitBtn = document.getElementById('submitBtn');
        var i = 1;

        fname.value = viewInfo['first_name'];
        lname.value = viewInfo['last_name'];
        bdate.value = viewInfo['birthdate'];
        studnumber.value = viewInfo['student_num'];
        address.value = viewInfo['home_address'];
        mobile.value = viewInfo['mobile_num'];
        email.value = viewInfo['email_add'];

        fname.setAttribute('required', '');
        lname.setAttribute('required', '');
        bdate.setAttribute('required', '');
        studnumber.setAttribute('readonly', '');
        address.setAttribute('required', '');
        mobile.setAttribute('required', '');
        email.setAttribute('required', '');

        for (i = 0; i < gender.length; i++) {
            if (gender[i].value == viewInfo['sex']) {
                gender[i].setAttribute('checked', '');
            }
        }

        for (i = 1; i < courseOpt.length; i++) {
            if (courseOpt[i].text == viewInfo['course']) {
                courseOpt[i].setAttribute('selected', '');
            }
        }

        for (i = 1; i < studentYear.length; i++) {
            if (studentYear[i].text == viewInfo['student_year']) {
                studentYear[i].setAttribute('selected', '');
            }
        }

        for (i = 1; i < semester.length; i++) {
            if (semester[i].text == viewInfo['school_term']) {
                semester[i].setAttribute('selected', '');
            }
        }

        // for (i = 1; i < student_section.length; i++) {
        //     if (student_section[i].text == viewInfo['section']) {
        //         student_section[i].setAttribute('selected', '');
        //     }
        // }

        submitBtn.innerHTML = 'Update';

        dynamicForm.setAttribute('action', 'updateRec.php');
        dynamicForm.addEventListener('submit', getThisID);

    }

    importForm.send();
}

function delThis(delID) {

    var dumpFName = delID.dataset.studentfname;
    var dumpLName = delID.dataset.studentlname;
    var dumpBdate = delID.dataset.studentbdate;
    var dumpDeleteID = delID.dataset.deleteid;
    var dumpCourse = delID.dataset.studentcourse;
    var dumpStudentYear = delID.dataset.studentyear;
    var dumpTerm = delID.dataset.schoolterm;
    // var dumpSection = delID.dataset.studentsection;
    var dumpAddr = delID.dataset.homeaddress;
    var dumpMobile = delID.dataset.studentmobile;
    var dumpEmail = delID.dataset.studentemail;

    var delForm = document.getElementById('delForm');
    var deleteID = document.getElementById('deleteID');
    var confirmDel = document.getElementById('confirmDel');

    confirmDel.innerHTML = '';
    
    var newDelPrompt = document.createElement('div');
    var newDelHead = document.createElement('h4');
    var newPar = document.createElement('p');
    var newDelFooter = document.createElement('p');
    var hrLn = document.createElement('hr');
    var dumpRec = document.createElement('ul');

    newDelPrompt.setAttribute('class', 'alert alert-warning');
    newDelPrompt.setAttribute('role', 'alert');
    newDelHead.setAttribute('class', 'alert-heading');
    newDelFooter.setAttribute('class', 'mb-2');
    dumpRec.setAttribute('class', 'list-group list-group-flush');
    dumpRec.innerHTML = 
    '<div class="input-group mb-3">' +
        '<span class="input-group-text">Student ID: </span>' +
        '<input type="text" class="form-control"' + 'value="' + dumpDeleteID + '"' + 'disabled' + '>' +
    '</div>' +
    '<div class="input-group mb-3">' +
        '<span class="input-group-text">Full name: </span>' +
        '<input type="text" class="form-control"' + 'value="' + dumpFName + '"' + 'disabled' + '>' +
        '<input type="text" class="form-control"' + 'value="' + dumpLName + '"' + 'disabled' + '>' +
    '</div>' +
    '<div class="input-group mb-3">' +
        '<span class="input-group-text">Birthdate: </span>' +
        '<input type="date" class="form-control"' + 'value="' + dumpBdate + '"' + 'disabled' + '>' +
    '</div>' + 
    '<div class="input-group mb-3">' +
        '<span class="input-group-text">Course: </span>' +
        '<input type="text" class="form-control"' + 'value="' + dumpCourse + '"' + 'disabled' + '>' +
    '</div>' + 
    '<div class="input-group mb-3">' +
        '<span class="input-group-text">Year & Term: </span>' +
        '<input type="text" class="form-control"' + 'value="' + dumpStudentYear + '"' + 'disabled' + '>' +
        '<input type="text" class="form-control"' + 'value="' + dumpTerm + '"' + 'disabled' + '>' +
    '</div>' + 
    // '<div class="input-group mb-3">' +
    //     '<span class="input-group-text">Section: </span>' +
    //     '<input type="text" class="form-control"' + 'value="' + dumpSection + '"' + 'disabled' + '>' +
    // '</div>' + 
    '<div class="input-group mb-3">' +
        '<span class="input-group-text">Home address: </span>' +
        '<textarea class="form-control" disabled>' + dumpAddr + '</textarea>' +
    '</div>' + 
    '<div class="input-group mb-3">' +
        '<span class="input-group-text">Contact details: </span>' +
        '<input type="text" class="form-control"' + 'value="' + dumpMobile + '"' + 'disabled' + '>' +
        '<input type="text" class="form-control"' + 'value="' + dumpEmail + '"' + 'disabled' + '>' +
    '</div>';

    newDelHead.innerHTML = 'CONFIRM DELETION';
    newPar.innerHTML = 'You are about to delete this record. Please note that this action is irreversible.';

    newDelPrompt.appendChild(newDelHead);
    newDelPrompt.appendChild(newPar);

    confirmDel.appendChild(newDelPrompt);
    confirmDel.appendChild(dumpRec);

    deleteID.setAttribute('value', delID.dataset.deleteid);

    delForm.addEventListener('submit', getThisID);

}

function getThisID(event) {

    var sendThis = new XMLHttpRequest;

    sendThis.open('POST', this.getAttribute('action'), true);

    sendThis.onload = function() {
        var sentInfo = JSON.parse(this.responseText);
    }
    
    sendThis.send();
}

function clearInput() {
    var clearInput = document.querySelectorAll('input[name="subjects"]:checked');
    var c = 0;

    while (c < clearInput.length) {
        clearInput[c].checked = false;
    }
}

function addDropSub(adInfo) {

    subjectsBox.innerHTML = '';
    subjectHeader.innerHTML = '';
    subjectTable.innerHTML = '';

    var addDropID = adInfo.dataset.addDrop;
    var newSubjectHeader = document.createElement('h5');

    for (var o in getInfo) {
        if (getInfo[o]['id'] == addDropID) {
            subjectStudID = '<h6>[' + getInfo[o]['student_num'] + ']' + ' ' + getInfo[o]['last_name'] + ' ' + getInfo[o]['first_name'] + '</h6>';
            console.log('subjectStudID', subjectStudID);
            subjectTableDesc.innerHTML = subjectStudID;
        }
    }

    newSubjectHeader.innerHTML = 'Add/drop subjects:';

    subjectHeader.className = 'mb-3';
    subjectHeader.appendChild(newSubjectHeader);

    for (z in allSubjects) {

        if (allSubjects[z]['subject_code'].startsWith('GE') || allSubjects[z]['subject_code'].startsWith('IT') || allSubjects[z]['subject_code'].startsWith('CS') || allSubjects[z]['subject_code'].startsWith('CE') || allSubjects[z]['subject_code'].startsWith('BA')) {

            genList(allSubjects[z], addDropID);

        }
    }
}

function genList(subjInfo, studID) {

    var checkID = studID;
    var checkSubj = subjInfo['id'];

    clearInput();

    var check = new XMLHttpRequest;

    check.open('GET', 'student/get_subject.php?student=' + checkID + '&subject=' + checkSubj, true);

    check.onload = function() {

        var isChecked = JSON.parse(this.responseText);

        var newDivList = document.createElement('div');
        var newListLabel = document.createElement('label');
        var newListSpan = document.createElement('span');
        var newInputList = document.createElement('input');

        newDivList.className = 'mb-3';
        newListLabel.className = 'list-group-item';
        newInputList.className = 'form-check-input me-1';
        newInputList.dataset.id = studID;
        
        newInputList.setAttribute('type', 'checkbox');
        newInputList.setAttribute('name', 'subjects');
        // newInputList.setAttribute('id', studID + subjInfo['id']);
        newInputList.setAttribute('data-add-drop', studID);
        newInputList.setAttribute('data-add-drop-subject', subjInfo['id']);
        newInputList.addEventListener('change', addSubject);

        if (isChecked == 'enrolled' && !newInputList.checked) {
            newInputList.checked = true;
        }

        if (isChecked == 'enrolled') {

            var newSubjectRow = document.createElement('tr');
            var newSubjectCode = document.createElement('td');
            var newSubjectTitle = document.createElement('td');
            var newSubjectLecturer = document.createElement('td');
            var newSubjectSchedule = document.createElement('td');
            var newSubjectYrSection = document.createElement('td');
            var newSubjectTerm  = document.createElement('td');

            newSubjectRow.setAttribute('id', checkID + subjInfo['id']);
            newSubjectCode.innerHTML = subjInfo['subject_code'];
            newSubjectTitle.innerHTML = subjInfo['subject_title'];
            newSubjectLecturer.innerHTML = subjInfo['lecturer_fname'] + ' ' + subjInfo['lecturer_lname'];
            newSubjectSchedule.innerHTML = subjInfo['sched_day'] + ' ' + subjInfo['sched_time'];
            newSubjectYrSection.innerHTML = subjInfo['student_year'] + ' ' + subjInfo['section'];
            newSubjectTerm.innerHTML = subjInfo['term'];

            newSubjectRow.appendChild(newSubjectCode);
            newSubjectRow.appendChild(newSubjectTitle);
            newSubjectRow.appendChild(newSubjectLecturer);
            newSubjectRow.appendChild(newSubjectSchedule);
            newSubjectRow.appendChild(newSubjectYrSection);
            newSubjectRow.appendChild(newSubjectTerm);
            subjectTable.appendChild(newSubjectRow);

        }

        newListSpan.innerHTML = subjInfo['subject_code'] + ' ' + subjInfo['subject_title']; 

        newListLabel.appendChild(newInputList);
        newListLabel.appendChild(newListSpan);
        newDivList.appendChild(newListLabel);

        subjectsBox.appendChild(newDivList);
    }

    check.send();
}

function addSubject(ad) {

    var toUpdateID = ad.target.dataset.addDrop;
    var toUpdateSubj = ad.target.dataset.addDropSubject;

    var passID = toUpdateID + toUpdateSubj;

    var manageSubject = new XMLHttpRequest;
    var params = toUpdateID + '&' + toUpdateSubj;

    if(ad.target.checked) {

        manageSubject.open('POST', 'student/update_subject.php', true);

        manageSubject.onload = function() {
            var updatedSub = JSON.parse(this.responseText);

            var updSubjectRow = document.createElement('tr');
            var updSubjectCode = document.createElement('td');
            var updSubjectTitle = document.createElement('td');
            var updSubjectLecturer = document.createElement('td');
            var updSubjectSchedule = document.createElement('td');
            var updSubjectYrSection = document.createElement('td');
            var updSubjectTerm  = document.createElement('td');

            updSubjectRow.setAttribute('id', passID);
            updSubjectCode.innerHTML = updatedSub['subject_code'];
            updSubjectTitle.innerHTML = updatedSub['subject_title'];
            updSubjectLecturer.innerHTML = updatedSub['lecturer_fname'] + ' ' + updatedSub['lecturer_lname'];
            updSubjectSchedule.innerHTML = updatedSub['sched_day'] + ' ' + updatedSub['sched_time'];
            updSubjectYrSection.innerHTML = updatedSub['student_year'] + ' ' + updatedSub['section'];
            updSubjectTerm.innerHTML = updatedSub['term'];

            updSubjectRow.appendChild(updSubjectCode);
            updSubjectRow.appendChild(updSubjectTitle);
            updSubjectRow.appendChild(updSubjectLecturer);
            updSubjectRow.appendChild(updSubjectSchedule);
            updSubjectRow.appendChild(updSubjectYrSection);
            updSubjectRow.appendChild(updSubjectTerm);
            subjectTable.appendChild(updSubjectRow);

        }

    } else {

        manageSubject.open('POST', 'student/delete_subject.php', true);

        manageSubject.onload = function() {
            var deletedSub = JSON.parse(this.responseText);

            if (subjectTable.childNodes.length > 0) {
                for (var t in subjectTable.childNodes) {
                    if (subjectTable.childNodes[t].id == passID) {
                        subjectTable.childNodes[t].remove();
                    }
                }
            }
                 
        }
    }

    manageSubject.send(JSON.stringify(params));

}

</script>
</body>
</html>