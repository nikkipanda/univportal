<?php

$method = $_SERVER['REQUEST_METHOD'];

$subject_prefix = $_GET['subject_prefix'] ?? null;
$q = $_GET['q'] ?? null;

if ($method === 'GET' && $subject_prefix) {

    require '../../database.php';

    $subject_prefix .= '%';

    $subject_get = $pdo->prepare('SELECT * FROM subject_term WHERE subject_code LIKE :subject_prefix');
    $subject_get->bindValue(':subject_prefix', $subject_prefix);

    $subject_get->execute();
    $subject_fetch = $subject_get->fetchAll(PDO::FETCH_ASSOC);

    if ($subject_fetch) {
        $subject_term_json = json_encode($subject_fetch, JSON_PRETTY_PRINT);

        echo $subject_term_json;
    }

    if (empty($subject_fetch)) {

        $subject_term_json = json_encode($subject_fetch, JSON_PRETTY_PRINT);

        echo $subject_term_json;
    }
}

else if ($method === 'GET' && $q) {

    require '../../database.php';

    $qry = '%'.$q.'%';

    $newQry = $pdo->prepare
    ('SELECT * FROM subject_term
    WHERE subject_code LIKE :qry
    OR subject_title LIKE :qry
    OR lecturer_lname LIKE :qry
    OR lecturer_fname LIKE :qry
    OR student_year LIKE :qry
    OR term LIKE :qry');
    
    $newQry->bindValue(':qry', $qry);

    $newQry->execute();
    $subject_fetch = $newQry->fetchAll(PDO::FETCH_ASSOC);

    $subject_term_json = json_encode($subject_fetch, JSON_PRETTY_PRINT);

    echo $subject_term_json;

}

else {

    require '../database.php';
    
    $subject_term = $pdo->prepare('SELECT * FROM subject_term');
    $subject_term->execute();
    $subject_term_arr = $subject_term->fetchAll(PDO::FETCH_ASSOC);
    
    $subject_term_json = json_encode($subject_term_arr, JSON_PRETTY_PRINT);

}

?>