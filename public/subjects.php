<?php

    session_start();

    if (!$_SESSION['userid']) {
        header('Location: ../admin/index.php');
    }
    
?>

<?php

    include_once '../partials/headers/header.php';
    require_once 'subject/get.php';

?>

<div class="toast" data-delay="8000" role="alert" aria-live="assertive" aria-atomic="true" id="successToast" style="position: fixed; bottom: 30px; left: 10px; z-index: 9;">
    <div class="toast-header">
        <img src="../images/icons/university.png" class="img-fluid rounded me-2" style="width: 30px;">
        <strong class="me-auto">Alert</strong>
        <small>just now</small>
        <button type="button" class="btn-close" data-bs-dismiss="toast" aria-label="Close"></button>
    </div>
    <div class="toast-body" id="successAlert"></div>
</div>
<div class="toast" data-delay="8000" role="alert" aria-live="assertive" aria-atomic="true" id="warningToast" style="position: fixed; bottom: 30px; left: 10px; z-index: 9;">
    <div class="toast-header">
        <img src="../images/icons/university.png" class="img-fluid rounded me-2" style="width: 30px;">
        <strong class="me-auto">Alert</strong>
        <small>just now</small>
        <button type="button" class="btn-close" data-bs-dismiss="toast" aria-label="Close"></button>
    </div>
    <div class="toast-body" id="warningAlert"></div>
</div>

<nav class="navbar sticky-top navbar-expand-lg navbar-light" style="background-color: rgb(234, 251, 255);">
    <div class="container-fluid">
        <a class="navbar-brand" href="#">Brand logo/name</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#toggleNav" aria-expanded="false">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="toggleNav">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                <li class="nav-item mx-2">
                    <a class="nav-link" aria-current="page" href="index.php">Home</a>
                </li>
                <li class="nav-item dropdown mx-2">
                    <a class="nav-link dropdown-toggle" href="#" id="toggleAxn" role="button" data-bs-toggle="dropdown" aria-expanded="false">Action</a>
                    <ul class="dropdown-menu" aria-labelledby="toggleAxn">
                        <li><a class="dropdown-item" href="create_rec.php">Create new record</a></li>
                        <li><hr class="dropdown-divider"></li>
                        <li><a class="dropdown-item" href="#">Dump</a></li>
                    </ul>
                </li>
                <li class="nav-item mx-2">
                    <a class="nav-link active" aria-current="page" href="subjects.php">Subjects</a>
                </li>
            </ul>
            <ul class="nav nav-pills">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" data-bs-toggle="dropdown" href="#" role="button" aria-expanded="false">Hi, <?php echo $_SESSION['adm-fname']; ?></a>
                    <ul class="dropdown-menu">
                    <li><a class="dropdown-item" href="#">Profile</a></li>
                    <li><a class="dropdown-item" href="#">Messages</a></li>
                    <li><a class="dropdown-item" href="#">Settings</a></li>
                    <li><a class="dropdown-item" href="#">Dark mode</a></li>
                    <li><hr class="dropdown-divider"></li>
                    <li><a class="dropdown-item" href="../admin/get/logout.php">Sign out</a></li>
                    </ul>
                </li>
            </ul>
            <form class="d-flex mt-3" action="subject/get.php" id="q">
                <input class="form-control me-2" type="search" name="q" placeholder="Enter keyword" aria-label="Search" id="qry">
                <button class="btn btn-outline-dark" type="submit">Search</button>
            </form>
        </div>
    </div>
</nav>
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-2 sidebar">
            <ul class="nav flex-column my-3">
                <div class="nav flex-column nav-pills nav-fill me-3" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                    <button class="nav-link active mb-2" id="v-pills-home-tab" data-bs-toggle="pill" data-bs-target="#v-pills-home" type="button" role="tab" aria-controls="v-pills-home" aria-selected="true">Dashboard</button>
                    <button class="nav-link mb-2" id="v-pills-table-tab" data-bs-toggle="pill" data-bs-target="#v-pills-table" type="button" role="tab" aria-controls="v-pills-table" aria-selected="false">Add record</button>
                    <h6 class="mt-3">Sort by college:</h6>
                    <button class="btn btn-outline-primary mb-2" id="v-pills-bsit-tab" data-filter="it" type="button">BSIT</button>
                    <button class="btn btn-outline-primary mb-2" id="v-pills-bscs-tab" data-filter="cs" type="button">BSCS</button>
                    <button class="btn btn-outline-primary mb-2" id="v-pills-bsce-tab" data-filter="ce" type="button">BSCE</button>
                    <button class="btn btn-outline-primary mb-2" id="v-pills-bsba-tab" data-filter="ba" type="button">BSBA</button>
                </div>
            </ul>
            <hr class="hrdivider">
            <h6 class="mt-3">Latest:</h6>
                <div class="col">
                    <div class="m-1 rounded row" style="border: 1px solid black;">Lorem ipsum dolor sit amet consectetur adipisicing elit. Facere, temporibus?</div>
                </div>
        </div>
        <div class="col-sm-10 py-5">
            <div class="tab-content" id="v-pills-tabContent">
                <div class="tab-pane fade show active" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">
                    <div class="alt-toast" id="altToast"></div>
                    <div class="row">
                        <div class="col-sm-9" id="innerNav">
                        </div>
                        <div class="col-sm-3">
                            <select class="form-select mb-3" id="recordsPerPage" onchange="changeRecNum(this)">
                                <option value="10" selected>10</option>
                                <option value="20">20</option>
                                <option value="50">50</option>
                            </select>
                        </div>
                    </div>
                    <div class="table-responsive mb-3">
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th scope="col">Subject Code</th>
                                    <th scope="col">Subject Title</th>
                                    <th scope="col">Lecturer</th>
                                    <th scope="col">Schedule</th>
                                    <th scope="col">Year</th>
                                    <th scope="col">Unit</th>
                                    <!-- <th scope="col">Date Created</th>
                                    <th scope="col">Date Updated</th> -->
                                    <th scope="col">Action</th>
                                </tr>
                            </thead>
                            <tbody id="tableRec" style="vertical-align: middle;"></tbody>
                        </table>
                    </div>
                    <div class="d-flex justify-content-center" id="footerID"></div>
                </div>
                <div class="tab-pane fade" id="v-pills-table" role="tabpanel" aria-labelledby="v-pills-table-tab">
                    <div class="container">
                        <?php
                            include_once '../partials/forms/subject_add.php';
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="updateRec" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Update</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
            </div>
        </div>
    </div>
</div>
<!-- modals !-->
<div class="modal fade" id="deleteRec" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Confirm deletion</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
            </div>
        </div>
    </div>
</div>

<script>
(function () {

var forms = document.querySelectorAll('.needs-validation');

Array.prototype.slice.call(forms).forEach(function (form) {

    form.addEventListener('submit', function (event) {

        if (!form.checkValidity()) {

            event.preventDefault();
            event.stopPropagation();

        }

        form.classList.add('was-validated')
    }, false);

})
})();

var subjectTerm = <?php echo $subject_term_json; ?>;
var successToast = document.getElementById('successToast');
var warningToast = document.getElementById('warningToast');
var q = document.getElementById('q');
var altToast = document.getElementById('altToast');
var subjectToast = document.getElementById('subjectToast');
var tableRec = document.getElementById('tableRec');
var recordsPerPage = document.getElementById('recordsPerPage');
var subjectTable = document.getElementById('subjectTable');
var pillsHome = document.getElementById('v-pills-home');
var filterDashB = document.getElementById('v-pills-home-tab');
var pillsTable = document.getElementById('v-pills-table-tab');
var tablePane = document.getElementById('v-pills-table');
var filterIT = document.getElementById('v-pills-bsit-tab');
var filterCS = document.getElementById('v-pills-bscs-tab');
var filterCE = document.getElementById('v-pills-bsce-tab');
var filterBA = document.getElementById('v-pills-bsba-tab');
var footerID = document.getElementById('footerID');
var chunk = [];

var st = new bootstrap.Toast(successToast);
var wt = new bootstrap.Toast(warningToast);

var subject_code = '<?php echo $_SESSION['subject_code'] ?? null ?>';
var subject_title = '<?php echo $_SESSION['subject_title'] ?? null ?>';

if (subject_code !== '' && subject_code !== null) {

    successAlert.innerHTML = 'Lastest successful entry: ' + subject_code + ' ' + subject_title;
    altToast.innerHTML= 'Lastest successful entry: ' + subject_code + ' ' + subject_title;

    st.show();
}

function clearToast() {

    <?php

        unset($_SESSION['subject_code']);
        unset($_SESSION['subject_title']);

    ?>
}

filterDashB.addEventListener('click', function() {

    location.reload();

});
pillsTable.addEventListener('click', function(t) {

    tablePane.setAttribute('class', 'tab-pane fade active');

})
filterIT.addEventListener('click', filterTable);
filterCS.addEventListener('click', filterTable);
filterCE.addEventListener('click', filterTable);
filterBA.addEventListener('click', filterTable);
q.addEventListener('submit', getThis);

function filterTable(e) {

    pillsHome.setAttribute('class', 'tab-pane fade show active');
    tablePane.setAttribute('class', 'tab-pane fade');

    var filterQry = new XMLHttpRequest;

    filterQry.open('GET', 'subject/get.php?subject_prefix=' + this.dataset.filter, true);

    filterQry.onload = function() {
        subjectTerm = JSON.parse(this.responseText);

        if (subjectTerm == null || subjectTerm == '') {
            warningAlert.innerHTML = 'Subject prefix '+ e.target.dataset.filter + ' did not match any record.';
            wt.show();   
        }

        if (subjectTerm !== '' && subjectTerm !== null) {
            chunk = subjectTerm.slice(0, recordsPerPage.value);
            genBtn(recordsPerPage.value);
            changePage();
        }
    }

    filterQry.send();

}

chunk = subjectTerm.slice(0, recordsPerPage.value);
genBtn(recordsPerPage.value);
changePage();

function genBtn(selectOpt) {

    footerID.innerHTML = '';

    var btnLen = Math.ceil(subjectTerm.length / selectOpt);

    if (subjectTerm.length > selectOpt) {

        for (var i = 1; i <= btnLen; i++) {
        
            var newBtn = document.createElement('button');
            newBtn.className = 'btn btn-primary mx-2';
            newBtn.setAttribute('id', i);
            newBtn.setAttribute('onclick', 'showRec(this)');
            newBtn.dataset.selectOpt = selectOpt;
            newBtn.innerHTML = i;
            footerID.append(newBtn);

        }
    }
}

function changeRecNum(e) {

    tableRec.innerHTML = '';
    footerID.innerHTML = '';
    chunk = subjectTerm.slice(0, e.value);
    genBtn(e.value);
    changePage();

}

function changePage() {

    tableRec.innerHTML = '';

    for (z in chunk) {

        var newRow = document.createElement('tr');
        var newCode = document.createElement('td');
        var newTitle = document.createElement('td');
        var newLecturer = document.createElement('td');
        var newSchedule  = document.createElement('td');
        var newYear = document.createElement('td');
        var newUnit = document.createElement('td');
        var newDateCreated = document.createElement('td');
        var newDateUpdated = document.createElement('td');
        var newAction = document.createElement('td');
        var newUpdateBtn = document.createElement('button');
        var newUpdIcon = document.createElement('img');
        var newDelBtn = document.createElement('button');
        var newDelIcon = document.createElement('img');

        newCode.innerHTML = chunk[z]['subject_code'];
        newTitle.innerHTML = chunk[z]['subject_title'];
        newLecturer.innerHTML = chunk[z]['lecturer_fname'] + ' ' + chunk[z]['lecturer_lname'];
        newSchedule.innerHTML = chunk[z]['sched_day'] + ' ' + chunk[z]['sched_time'];
        newYear.innerHTML = chunk[z]['student_year'];
        newUnit.innerHTML = chunk[z]['unit'];
        
        newUpdIcon.setAttribute('src', '../images/icons/edit.png');
        newUpdIcon.style.width = '30px';
        newUpdIcon.style.height = 'auto';
        newUpdateBtn.appendChild(newUpdIcon);
        newUpdateBtn.setAttribute('type', 'button');
        newUpdateBtn.setAttribute('class', 'btn m-1');
        newUpdateBtn.setAttribute('data-bs-toggle', 'modal');
        newUpdateBtn.setAttribute('data-bs-target', '#updateRec');
        
        newDelIcon.setAttribute('src', '../images/icons/delete.png');
        newDelIcon.style.width = '30px';
        newDelIcon.style.height = 'auto';
        newDelBtn.appendChild(newDelIcon);
        newDelBtn.setAttribute('type', 'button');
        newDelBtn.setAttribute('class', 'btn m-1');
        newDelBtn.setAttribute('data-bs-toggle', 'modal');
        newDelBtn.setAttribute('data-bs-target', '#deleteRec');

        newRow.appendChild(newCode);
        newRow.appendChild(newTitle);
        newRow.appendChild(newLecturer);
        newRow.appendChild(newSchedule);
        newRow.appendChild(newYear);
        newRow.appendChild(newUnit);
        newAction.appendChild(newUpdateBtn);
        newAction.appendChild(newDelBtn);
        newRow.appendChild(newAction);

        tableRec.appendChild(newRow);

    }
}

function showRec(btn) {

    var selectedVal = btn.dataset.selectOpt;

    if (subjectTerm.length < (selectedVal * btn.id)) {
        chunk = subjectTerm.slice((selectedVal * btn.id) - selectedVal);
    } else if (subjectTerm.length >= (selectedVal * btn.id)) {
        chunk = subjectTerm.slice((selectedVal * btn.id) - selectedVal, (selectedVal * btn.id));
    }

    changePage(chunk);

}

function getThis(e) {

    e.preventDefault();

    var thisVal = e.target.qry.value;

    tableRec.innerHTML = '';

    var sendThis = new XMLHttpRequest;

    sendThis.open('GET', this.getAttribute('action') + '?q=' + thisVal, true);

    sendThis.onload = function() {
        subjectTerm = JSON.parse(this.responseText);

        chunk = subjectTerm.slice(0, recordsPerPage.value);
        genBtn(recordsPerPage.value);
        changePage();

    }

    sendThis.send();
}

</script>
</body>
</html>