<?php

    include_once '../../partials/headers/header.php';

?>

<?php

    session_start();

    if(!$_SESSION['usr_handle']) {
        header('Location: ../../user/index.php');
    }

?>

<nav class="navbar sticky-top navbar-expand-lg navbar-light" style="background-color: rgb(234, 251, 255);">
    <div class="container-fluid">
        <a class="navbar-brand" href="index.php">Brand logo/name</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#toggleNav" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="toggleNav">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                <li class="nav-item">
                    <a class="nav-link active" aria-current="page" href="index.php">Home</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="toggleAxn" role="button" data-bs-toggle="dropdown" aria-expanded="false">Action</a>
                    <ul class="dropdown-menu" aria-labelledby="toggleAxn">
                        <li><a class="dropdown-item" href="#">#</a></li>
                        <li><hr class="dropdown-divider"></li>
                        <li><a class="dropdown-item" href="#">Dump</a></li>
                    </ul>
                </li>
                <li class="nav-item">
                    <a class="nav-link active" aria-current="page" href="#">F.A.Q.</a>
                </li>   
            </ul>
            <ul class="nav nav-pills">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" data-bs-toggle="dropdown" href="#" role="button" aria-expanded="false">Hi, <?php echo $_SESSION['usr_fname'] ?></a>
                    <ul class="dropdown-menu">
                        <li><a class="dropdown-item" href="#">Profile</a></li>
                        <li><a class="dropdown-item" href="#">Messages</a></li>
                        <li><a class="dropdown-item" href="#">Settings</a></li>
                        <li><a class="dropdown-item" href="#">Dark mode</a></li>
                        <li><hr class="dropdown-divider"></li>
                        <li><a class="dropdown-item" href="../../user/get/logout.php">Sign out</a></li>
                    </ul>
                </li>
            </ul>
            <form class="d-flex mt-3" action="" id="q">
                <input class="form-control me-2" type="search" name="" placeholder="Enter keyword" aria-label="Search" id="qry">
                <button class="btn btn-outline-dark" type="submit">Search</button>
            </form>
        </div>
    </div>
</nav>
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-3 sidebar">
            <ul class="nav flex-column my-3">
                <div class="nav flex-column nav-pills nav-fill me-3" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                    <button class="nav-link active" id="v-pills-home-tab" data-bs-toggle="pill" data-bs-target="#v-pills-home" type="button" role="tab" aria-controls="v-pills-home" aria-selected="true">Home</button>
                    <button class="nav-link" id="v-pills-grades-tab" data-bs-toggle="pill" data-bs-target="#v-pills-grades" type="button" role="tab" aria-controls="v-pills-grades" aria-selected="false">Grades</button>
                    <button class="nav-link" id="v-pills-subjects-tab" data-bs-toggle="pill" data-bs-target="#v-pills-subjects" type="button" role="tab" aria-controls="v-pills-subjects" aria-selected="false">Subjects</button>
                    <button class="nav-link" id="v-pills-fdback-tab" data-bs-toggle="pill" data-bs-target="#v-pills-fdback" type="button" role="tab" aria-controls="v-pills-fdback" aria-selected="false">Feedback</button>
                </div>
            </ul>
            <hr class="hrdivider">
        </div>
        <div class="col-sm-9 py-5">
            <div class="tab-content" id="v-pills-tabContent">
                <div class="tab-pane fade show active" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">
                    Lorem ipsum dolor, sit amet consectetur adipisicing elit. Necessitatibus provident rerum nobis unde ipsum perspiciatis quasi reprehenderit veritatis minus tempore consequuntur illum officia molestias nihil et enim repellendus iste quos sapiente, sit fugit natus tenetur ex! Maiores dolorem consequatur nulla blanditiis eveniet cumque sequi. Rem est deleniti laboriosam eligendi eius.
                </div>
                <div class="tab-pane fade" id="v-pills-grades" role="tabpanel" aria-labelledby="v-pills-grades-tab">
                    Lorem ipsum dolor sit, amet consectetur adipisicing elit. Quia illum soluta expedita, possimus ratione sunt veritatis! Tempora nobis incidunt reiciendis?
                </div>
                <div class="tab-pane fade" id="v-pills-subjects" role="tabpanel" aria-labelledby="v-pills-subjects-tab">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugit corrupti numquam dolor est quaerat animi, temporibus aliquid ex nisi ipsa?
                </div>
                <div class="tab-pane fade" id="v-pills-fdback" role="tabpanel" aria-labelledby="v-pills-fdback-tab">
                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Quaerat quos nostrum nesciunt est facilis. Mollitia esse quis totam tenetur earum. Obcaecati voluptate alias quisquam dolorem molestiae nesciunt pariatur blanditiis magni.</div>
            </div>
        </div>
    </div>
</div>


<script>

function openNav() {
  document.getElementById("test").style.width = "250px";
}

/* Set the width of the sidebar to 0 (hide it) */
function closeNav() {
  document.getElementById("test").style.width = "0";
}

</script>

</body>
</html>