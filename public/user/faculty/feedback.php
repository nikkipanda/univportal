<?php

    include_once '../../../partials/headers/header.php';

?>

<?php

    session_start();

    if(!$_SESSION['usr_handle']) {
        header('Location: ../../user/index.php');
    }

?>

<nav class="navbar sticky-top navbar-expand-lg navbar-light" style="background-color: rgb(234, 251, 255);">
    <div class="container-fluid">
        <a class="navbar-brand" href="index.php">Brand logo/name</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#toggleNav" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="toggleNav">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                <li class="nav-item">
                    <a class="nav-link active" aria-current="page" href="index.php">Dashboard</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="toggleAxn" role="button" data-bs-toggle="dropdown" aria-expanded="false">Action</a>
                    <ul class="dropdown-menu" aria-labelledby="toggleAxn">
                        <li><a class="dropdown-item" href="schedule.php">Schedule</a></li>
                        <li><a class="dropdown-item" href="class_list.php">Class List</a></li>
                        <li><a class="dropdown-item" href="grades.php">Grades</a></li>
                        <li><a class="dropdown-item" href="feedback.php">Feedback</a></li>
                        <li><hr class="dropdown-divider"></li>
                        <li><a class="dropdown-item" href="#">Dump</a></li>
                    </ul>
                </li>
                <li class="nav-item">
                    <a class="nav-link active" aria-current="page" href="#">F.A.Q.</a>
                </li>   
            </ul>
            <ul class="nav nav-pills">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" data-bs-toggle="dropdown" href="#" role="button" aria-expanded="false">Hi, <?php echo $_SESSION['usr_fname'] ?></a>
                    <ul class="dropdown-menu">
                        <li><a class="dropdown-item" href="#">Profile</a></li>
                        <li><a class="dropdown-item" href="#">Messages</a></li>
                        <li><a class="dropdown-item" href="#">Settings</a></li>
                        <li><a class="dropdown-item" href="#">Dark mode</a></li>
                        <li><hr class="dropdown-divider"></li>
                        <li><a class="dropdown-item" href="../../user/get/logout.php">Sign out</a></li>
                    </ul>
                </li>
            </ul>
            <form class="d-flex mt-3" action="" id="q">
                <input class="form-control me-2" type="search" name="" placeholder="Enter keyword" aria-label="Search" id="qry">
                <button class="btn btn-outline-dark" type="submit">Search</button>
            </form>
        </div>
    </div>
</nav>
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-3 sidebar">
            <h5 class="mt-3">What's new</h6>
            <div class="list-group">
                <a href="#" class="list-group-item list-group-item-action" aria-current="true">
                    <div class="d-flex w-100 justify-content-between">
                        <h5 class="mb-1">List group item heading</h5>
                        <small>3 days ago</small>
                    </div>
                    <p class="mb-1">Some placeholder content in a paragraph.</p>
                    <small>And some small print.</small>
                </a>
                <a href="#" class="list-group-item list-group-item-action">
                    <div class="d-flex w-100 justify-content-between">
                        <h5 class="mb-1">List group item heading</h5>
                        <small class="text-muted">3 days ago</small>
                    </div>
                    <p class="mb-1">Some placeholder content in a paragraph.</p>
                    <small class="text-muted">And some muted small print.</small>
                </a>
                <a href="#" class="list-group-item list-group-item-action">
                    <div class="d-flex w-100 justify-content-between">
                        <h5 class="mb-1">List group item heading</h5>
                        <small class="text-muted">3 days ago</small>
                    </div>
                    <p class="mb-1">Some placeholder content in a paragraph.</p>
                    <small class="text-muted">And some muted small print.</small>
                </a>
            </div>            
        </div>
        <div class="col-sm-9 py-5">
            <div class="tab-content" id="v-pills-tabContent">
                <div class="tab-pane fade show active" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">
                    Lorem ipsum dolor, sit amet consectetur adipisicing elit. Necessitatibus provident rerum nobis unde ipsum perspiciatis quasi reprehenderit veritatis minus tempore consequuntur illum officia molestias nihil et enim repellendus iste quos sapiente, sit fugit natus tenetur ex! Maiores dolorem consequatur nulla blanditiis eveniet cumque sequi. Rem est deleniti laboriosam eligendi eius.
                </div>
                <div class="tab-pane fade" id="v-pills-grades" role="tabpanel" aria-labelledby="v-pills-grades-tab">
                    Lorem ipsum dolor sit, amet consectetur adipisicing elit. Quia illum soluta expedita, possimus ratione sunt veritatis! Tempora nobis incidunt reiciendis?
                </div>
                <div class="tab-pane fade" id="v-pills-subjects" role="tabpanel" aria-labelledby="v-pills-subjects-tab">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugit corrupti numquam dolor est quaerat animi, temporibus aliquid ex nisi ipsa?
                </div>
                <div class="tab-pane fade" id="v-pills-fdback" role="tabpanel" aria-labelledby="v-pills-fdback-tab">
                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Quaerat quos nostrum nesciunt est facilis. Mollitia esse quis totam tenetur earum. Obcaecati voluptate alias quisquam dolorem molestiae nesciunt pariatur blanditiis magni.</div>
            </div>
        </div>
    </div>
</div>


<script>

function openNav() {
  document.getElementById("test").style.width = "250px";
}

/* Set the width of the sidebar to 0 (hide it) */
function closeNav() {
  document.getElementById("test").style.width = "0";
}

</script>

</body>
</html>