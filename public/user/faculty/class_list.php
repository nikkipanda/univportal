<?php

    session_start();

    if (!$_SESSION['usr_handle']) {
        header('Location: ../../user/index.php');
    }

?>

<?php

    include_once '../../../partials/headers/user/sub_pages/header.php';
    require_once 'get/class_list_rec.php';

?>

<nav class="navbar sticky-top navbar-expand-lg navbar-light" style="background-color: rgb(234, 251, 255);">
    <div class="container-fluid">
        <a class="navbar-brand" href="../lecturer.php">Brand logo/name</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#toggleNav" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="toggleNav">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                <li class="nav-item">
                    <a class="nav-link active" aria-current="page" href="../lecturer.php">Dashboard</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="toggleAxn" role="button" data-bs-toggle="dropdown" aria-expanded="false">Action</a>
                    <ul class="dropdown-menu" aria-labelledby="toggleAxn">
                        <li><a class="dropdown-item" href="schedule.php">Schedule</a></li>
                        <li><a class="dropdown-item" href="class_list.php">Class List</a></li>
                        <li><a class="dropdown-item" href="feedback.php">Feedback</a></li>
                        <li><hr class="dropdown-divider"></li>
                        <li><a class="dropdown-item" href="#">Dump</a></li>
                    </ul>
                </li>
                <li class="nav-item">
                    <a class="nav-link active" aria-current="page" href="#">F.A.Q.</a>
                </li>   
            </ul>
            <ul class="nav nav-pills">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" data-bs-toggle="dropdown" href="#" role="button" aria-expanded="false">Hi, <?php echo $_SESSION['usr_fname'] ?></a>
                    <ul class="dropdown-menu">
                        <li><a class="dropdown-item" href="#">Profile</a></li>
                        <li><a class="dropdown-item" href="#">Messages</a></li>
                        <li><a class="dropdown-item" href="#">Settings</a></li>
                        <li><a class="dropdown-item" href="#">Dark mode</a></li>
                        <li><hr class="dropdown-divider"></li>
                        <li><a class="dropdown-item" href="../../../user/get/logout.php">Sign out</a></li>
                    </ul>
                </li>
            </ul>
            <form class="d-flex mt-3" action="" id="q">
                <input class="form-control me-2" type="search" name="" placeholder="Enter keyword" aria-label="Search" id="qry">
                <button class="btn btn-outline-dark" type="submit">Search</button>
            </form>
        </div>
    </div>
</nav>
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-2 sidebar">
            <div class="d-grid">
                <button class="btn rounded btn-primary my-4" data-subject-code="all" id="allRec">All records</button>
            </div>
        <h5 class="mt-4 mb-2">Sort by subject</h5>
        <div class="d-flex btn-group btn-group-vertical" role="group" id="sortSubject"></div>
        </div>
        <div class="col-sm-10 py-5">
            <div class="container">
                <div class="row">
                    <div class="col-sm-9" id="recordCount"></div>
                    <div class="col-sm-3 mb-3">
                    <select class="form-select" id="recordsPerPage" onchange="changeRecNum(this)">
                        <option value="10" selected>10</option>
                        <option value="20">20</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                        <option value="150">150</option>
                        <option value="200">200</option>
                    </select>
                    </div>
                </div>
                <div class="table-responsive mb-3">
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <th scope="col" style="width: 14.29%;">Student #</th>
                                <th scope="col" style="width: 20.29%;">Last Name</th>
                                <th scope="col" style="width: 20.29%;">First Name</th>
                                <th scope="col" style="width: 10.29%;">Course</th>
                                <th scope="col" style="width: 14.29%;">Year</th>
                                <th scope="col" style="width: 10.29%;">Section</th>
                                <th scope="col" style="width: 10.29%;">Grade</th>
                            </tr>
                        </thead>
                        <tbody id="tableRec" style="vertical-align: middle;"></tbody>
                    </table>
                </div>
                <div class="d-flex justify-content-center" id="footerID"></div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="gradeRec" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="gradeInfo"></h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close" onclick="clearInput()"></button>
            </div>
            <div class="modal-body">
                <div id="gradeAlertBox"></div>
                <div id="gradeBox">
                    <?php
                        include_once '../../../partials/forms/update_grades.php';
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
var allClass = <?php echo $class_json_arr; ?>;
var section = document.getElementById('section');
var allBtn = document.getElementById('allBtn');
var tableRec = document.getElementById('tableRec');
var recordsPerPage = document.getElementById('recordsPerPage');
var recordChunk = Math.ceil(allClass.length / recordsPerPage.value);
var recordCount = document.getElementById('recordCount');
var sortSubject = document.getElementById('sortSubject');
var countRow = 'Showing ' + '<b>' + allClass.length + '</b>';
var gradeInfo = document.getElementById('gradeInfo');
var gradeAlertBox = document.getElementById('gradeAlertBox');
var updateGrade = document.getElementById('updateGrade');
// var quiz1 = document.getElementById('quiz1');
// var seatwork1 = document.getElementById('seatwork1');
// var homework1 = document.getElementById('homework1');
var allRec = document.getElementById('allRec');
var gradePrev = document.getElementById('gradePrev');
var getValues = document.getElementById('updateGrade');
var addonQuiz = document.getElementById('quiz');
var addonSw = document.getElementById('seatwork');
var addonHw = document.getElementById('homework');

var ctr = 0;
var qTotal = 0;
var swTotal = 0;
var hwTotal = 0;
var addonArr = [];
var fieldTarget = parseFloat(ctr);

console.log('field: ', fieldTarget);

allRec.addEventListener('click', getSubject);
updateGrade.addEventListener('submit', submitGrade);

sortSub();

chunk = allClass.slice(0, recordsPerPage.value);
genBtn(recordsPerPage.value, allClass.length);
changePage();

if (allClass.length > 1) {
    recordCount.innerHTML = countRow + ' records:';
} else if (newArr == 1) {
    recordCount.innerHTML = countRow + ' record:';
} else {
    recordCount.innerHTML = 'No record to show.';
}

function changeRecNum(e) {
    tableRec.innerHTML = '';
    chunk = allClass.slice(0, e.value);

    genBtn(e.value);
    changePage();
}

function genBtn(selectOpt, len) {
    footerID.innerHTML = '';

    recordChunk = Math.ceil(len / selectOpt);

    if (len > selectOpt) {
        for (var i = 1; i <= recordChunk; i++) {
            var newBtn = document.createElement('button');
            newBtn.className = 'btn btn-primary mx-2';
            newBtn.setAttribute('id', i);
            newBtn.setAttribute('onclick', 'showRec(this)');
            newBtn.dataset.selectOpt = selectOpt;
            newBtn.innerHTML = i;
            footerID.append(newBtn);
        }
    }
}

function changePage() {
    tableRec.innerHTML = '';

    console.log('chunk', chunk);

    for (z in chunk) {
        var newRow = document.createElement('tr');
        var newID = document.createElement('td');
        var newLName = document.createElement('td');
        var newFName = document.createElement('td');
        var newCourse = document.createElement('td');
        var newYear  = document.createElement('td');
        var newSection = document.createElement('td');
        var newAction = document.createElement('td');
        var newGradeBtn = document.createElement('button');
        var newGradeIcon = document.createElement('img');

        newID.style.fontWeight = '600';

        newGradeBtn.setAttribute('onclick', 'viewThis(this)');
        newGradeBtn.setAttribute('type', 'button');
        newGradeBtn.setAttribute('class', 'btn m-1');
        newGradeBtn.setAttribute('data-bs-toggle', 'modal');
        newGradeBtn.setAttribute('data-bs-target', '#gradeRec');
        newGradeBtn.setAttribute('id', chunk[z][0]['student_num']);
        newGradeBtn.dataset.gradeId = chunk[z][0]['id'];
        newGradeBtn.dataset.lname = chunk[z][0]['last_name'];
        newGradeBtn.dataset.fname = chunk[z][0]['first_name'];
        newGradeIcon.setAttribute('src', '../../../images/icons/grade.png');
        newGradeIcon.style.width = '25px';
        newGradeIcon.style.height = 'auto';
        newGradeBtn.appendChild(newGradeIcon);

        newID.innerHTML = chunk[z][0]['student_num'];
        newLName.innerHTML = chunk[z][0]['last_name'];
        newFName.innerHTML = chunk[z][0]['first_name'];
        newCourse.innerHTML = chunk[z][0]['course'];
        newYear.innerHTML = chunk[z][0]['student_year'];
        newSection.innerHTML = chunk[z][0]['section'];

        newRow.appendChild(newID);
        newRow.appendChild(newLName);
        newRow.appendChild(newFName);
        newRow.appendChild(newCourse);
        newRow.appendChild(newYear);
        newRow.appendChild(newSection);
        newAction.appendChild(newGradeBtn);
        newRow.appendChild(newAction);

        tableRec.appendChild(newRow);
    }
}

function showRec(ID) {
    var selectedVal = ID.dataset.selectOpt;
    tableRec.innerHTML = '';

    if (allClass.length < (selectedVal * ID.id)) {
        chunk = allClass.slice((selectedVal * ID.id) - selectedVal);
    } else if (allClass.length >= (selectedVal * ID.id)) {
        chunk = allClass.slice((selectedVal * ID.id) - selectedVal, (selectedVal * ID.id));
    }

    changePage();
}

function sortSub() {
    var sortSub = new XMLHttpRequest;

    sortSub.open('GET', 'get/class_list_rec.php?sort_subject=true', true);
    sortSub.onload = function() {
        var subjects = JSON.parse(this.responseText);
        var newSub = [];

        for (var s = 0; s < subjects.length; s++) {
            var newSortBtn = document.createElement('button');
        
            newSortBtn.className = 'btn rounded btn-outline-primary mb-2';
            newSortBtn.dataset.subjectId = subjects[s]['id'];
            newSortBtn.dataset.subjectSection = subjects[s]['section'];
            newSortBtn.innerHTML = subjects[s]['subject_title'];
            newSortBtn.addEventListener('click', getSubject);

            sortSubject.appendChild(newSortBtn);  
        }
    }
    sortSub.send();
}

function getSubject(g) {
    var newArr = [];

    if (g.target.dataset.subjectCode == 'all') {
        location.reload();
    } else {
        for (var u in allClass) {
            if (allClass[u][0]['subject_term'] == g.target.dataset.subjectId) {
                newArr.push(allClass[u]);
            }
        }
    }
    chunk = newArr.slice(0, recordsPerPage.value);
    genBtn(recordsPerPage.value, newArr.length);
    changePage();

    countRow = 'Showing ' + '<b>' + newArr.length + '</b>';

    if (newArr.length > 1) {
        recordCount.innerHTML = countRow + ' records for <b>section ' + g.target.dataset.subjectId + '</b>:';
    } else if (newArr == 1) {
        recordCount.innerHTML = countRow + ' record for <b>section ' + g.target.dataset.subjectId + '</b>:';
    } else {
        recordCount.innerHTML = 'No record to show for <b>section ' + g.target.dataset.subjectId + '</b>.';
    }
}

function viewThis(v) {
    clearInput();

    console.log('this ID grade: ', v.id, v.dataset.gradeId);
    gradeAlertBox.innerHTML = '';
    gradeInfo.innerHTML = 'Update grade for [&#xa0;Student&#xa0;#&#xa0;' + v.id + '&#xa0;]' + '&#xa0;' + v.dataset.lname + ',&#xa0;' + v.dataset.fname;

    var gradeAlert = document.createElement('div');
    var gradeHeader = document.createElement('h4');
    var gradeAlertDesc = document.createElement('p');

    gradeHeader.innerHTML = 'Note:'
    gradeAlertDesc.innerHTML = 'You are allowed to submit the grades only <b>once</b>. Make sure the details are accurate before you submit; otherwise, please contact the school registrar.';

    gradeAlert.className = 'alert alert-warning';
    gradeAlert.setAttribute('role', 'alert');

    gradeAlert.appendChild(gradeHeader);
    gradeAlert.appendChild(gradeAlertDesc);
    gradeAlertBox.appendChild(gradeAlert);

    updateGrade.dataset.targetId = v.dataset.gradeId;
}

function addFieldValidate(f) {

    var thisGradeType = (f.dataset.gradeType + 1).toLowerCase();
    var thisID = document.getElementById(thisGradeType);

    console.log('thisID: ', thisID);

    if (!isNaN(parseInt(thisID.value, 10))) {
        addField(f);
    }  else {
        console.log('CANNOT VALIDATE: ', parseInt(thisID.value, 10));
    }
}

function addField(d) {

    console.log('seatwork: ', document.getElementById('seatwork1').value);

    console.log('Add field: ', d.dataset.gradeType);
    var gradeType = d.dataset.gradeType;
    var addon = ['homework', 'quiz', 'seatwork'];
    var addType = ++fieldTarget;
    var newFieldDiv = document.createElement('div');
    var newField = document.createElement('input');
    var newGradeVd = document.createElement('div');
    var newDelField = document.createElement('button');

    newFieldDiv.setAttribute('id', gradeType + addType);
    newFieldDiv.className = 'input-group mb-3';

    newField.className = 'form-control';
    newField.setAttribute('type', 'number');
    newField.setAttribute('min', '60');
    newField.setAttribute('max', '100');
    newField.setAttribute('step', '2');
    newField.setAttribute('name', gradeType + addType);
    newField.dataset.targetCateg = gradeType.toLowerCase();
    newField.setAttribute('required', '');
    newField.addEventListener('blur', addonPrev);

    newGradeVd.className = 'invalid-feedback';
    newGradeVd.innerHTML = d.dataset.gradeType + ' cannot be empty.';

    newDelField.dataset.inputTarget = gradeType + addType;
    newDelField.className = 'btn btn-outline-danger';
    newDelField.setAttribute('type', 'button');
    newDelField.addEventListener('click', popField);
    newDelField.innerHTML = 'X';

    newFieldDiv.appendChild(newField);
    newFieldDiv.appendChild(newDelField);
    newFieldDiv.appendChild(newGradeVd);

    for (ctr in addon) {
        if (gradeType.toLowerCase() == addon[ctr]) {
            document.getElementById(addon[ctr]).appendChild(newFieldDiv);
        }
    }

}

function popField(p) {
    console.log('Del field dataset: ', p.target.dataset.inputTarget);
    document.getElementById(p.target.dataset.inputTarget).remove();

}

function previewThis(w) {
    console.log('w.name ', w.name);
    var addPrev = gradePrev.querySelector(`tr > td[data-target-categ=${w.name}]`);

    addPrev.innerHTML = w.value;

}

function addonPrev(o) { 

    // when user delete a field, remove its dom on gradePrev tbl
    // 

    var newAddonPrevCtr = ctr;

    console.log('newCtr: ', newAddonPrevCtr++);

    addonArr = [];
    var targetCateg = o.target.dataset.targetCateg;
    console.log('targetCateg: ', targetCateg);
    var prevQuiz = addonQuiz.querySelectorAll('input');
    var prevSw = addonSw.querySelectorAll('input');
    var prevHw = addonHw.querySelectorAll('input');
    var replaceCell = gradePrev.querySelector(`td[data-target-categ=${targetCateg+1}`);
    const accVal = parseInt(replaceCell.innerText, 10);
    console.log('first accVal: ', accVal);

    if (targetCateg == 'quiz') {
        for (ctr in prevQuiz) {
            if (!isNaN(parseInt(prevQuiz[ctr].value, 10))) {
                addonArr.push(parseInt(prevQuiz[ctr].value, 10));
            }
        }
    } else if (targetCateg == 'seatwork') {
        for (ctr in prevSw) {
            if (!isNaN(parseInt(prevSw[ctr].value, 10))) {
                addonArr.push(parseInt(prevSw[ctr].value, 10));
            }
        }
    } else if (targetCateg == 'homework') {
        for (ctr in prevHw) {
            if (!isNaN(parseInt(prevHw[ctr].value, 10))) {
                addonArr.push(parseInt(prevHw[ctr].value, 10));
            }
        }
    }
    console.log('targetCateg addon arr: ', targetCateg, addonArr);
    var reduceArr = addonArr.reduce(function(acc, current) {
        console.log('acc: ', acc);
        console.log('current: ', current);
        return acc + current;
    }, 0);
    console.log('2nd accval: ', accVal);
    console.log('reduce: ', reduceArr);
    replaceCell.innerHTML = accVal + reduceArr;
    console.log('replaceCell.innerHTML: ', replaceCell.innerHTML);
}

function submitGrade(s) {
    s.preventDefault();

    var updateGradeID = updateGrade.dataset.targetId;
    var getAttendance = getValues.querySelector('input[name="attendance"]');
    var getRecitation = getValues.querySelector('input[name="recitation"]');
    var getPrelim = getValues.querySelector('input[name="prelim"]');
    var getMidterm = getValues.querySelector('input[name="midterm"]');
    var getFinal = getValues.querySelector('input[name="final"]');
    var getQuiz = getValues.querySelector('input[name="quiz1"]');
    var getSw = getValues.querySelector('input[name="seatwork1"]');
    var getHw = getValues.querySelector('input[name="homework1"]');

    var getAllQz = addonQuiz.querySelectorAll('input');
    var getAllSw = addonSw.querySelectorAll('input');
    var getAllHw = addonHw.querySelectorAll('input');

    console.log('this id', updateGradeID);

    var params = {
        id: updateGradeID,
        attendance: getAttendance.value,
        recitation: getRecitation.value,
        prelim: getPrelim.value,
        midterm: getMidterm.value,
        final: getFinal.value,
        quiz: [
            getQuiz.value,
        ],
        seatwork: [
            getSw.value,
        ],
        homework: [
            getHw.value,
        ],
    };

    console.log('quiz:', getAllQz);
    console.log('sw:', getAllSw);
    console.log('hw:', getAllHw);

    if (getAllQz.length !== 0) {
        for (ctr = 0; ctr < getAllQz.length; ctr++) {
            params.quiz.push(getAllQz[ctr].value);
        }
    }

    if (getAllSw.length !== 0) {
        for (ctr = 0; ctr < getAllSw.length; ctr++) {
            params.seatwork.push(getAllSw[ctr].value);
        }
    }

    if (getAllHw.length !== 0) {
        for (ctr = 0; ctr < getAllHw.length; ctr++) {
            params.homework.push(getAllHw[ctr].value);
        }
    }

    var newGrade = new XMLHttpRequest;
    newGrade.open('POST', this.getAttribute('action'), true);
    newGrade.onload = function() {
        console.log('submit triggered: ', this.responseText);
    }
    newGrade.send(JSON.stringify(params));

    console.log('params: ', params);
}

function clearInput() {
    var inputFields = ['attendance', 'recitation', 'homework1', 'quiz1', 'seatwork1', 'prelim', 'midterm', 'final'];
    ctr = 0;

    for (ctr in inputFields) {
        var clearInput = updateGrade.querySelector(`input[name=${inputFields[ctr]}`);
        var gradePrevInput = gradePrev.querySelector(`td[data-target-categ=${inputFields[ctr]}`);
        clearInput.value = '';
        gradePrevInput.innerHTML = '';
    }
}
</script>
</body>
</html>