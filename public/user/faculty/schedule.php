<?php

    include_once '../../../partials/headers/user/sub_pages/header.php';

?>

<?php

    session_start();

    if(!$_SESSION['usr_handle']) {
        header('Location: ../../user/index.php');
    }

?>

<nav class="navbar sticky-top navbar-expand-lg navbar-light" style="background-color: rgb(234, 251, 255);">
    <div class="container-fluid">
        <a class="navbar-brand" href="../lecturer.php">Brand logo/name</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#toggleNav" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="toggleNav">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                <li class="nav-item">
                    <a class="nav-link active" aria-current="page" href="../lecturer.php">Dashboard</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="toggleAxn" role="button" data-bs-toggle="dropdown" aria-expanded="false">Action</a>
                    <ul class="dropdown-menu" aria-labelledby="toggleAxn">
                        <li><a class="dropdown-item" href="schedule.php">Schedule</a></li>
                        <li><a class="dropdown-item" href="class_list.php">Class List</a></li>
                        <li><a class="dropdown-item" href="feedback.php">Feedback</a></li>
                        <li><hr class="dropdown-divider"></li>
                        <li><a class="dropdown-item" href="#">Dump</a></li>
                    </ul>
                </li>
                <li class="nav-item">
                    <a class="nav-link active" aria-current="page" href="#">F.A.Q.</a>
                </li>   
            </ul>
            <ul class="nav nav-pills">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" data-bs-toggle="dropdown" href="#" role="button" aria-expanded="false">Hi, <?php echo $_SESSION['usr_fname'] ?></a>
                    <ul class="dropdown-menu">
                        <li><a class="dropdown-item" href="#">Profile</a></li>
                        <li><a class="dropdown-item" href="#">Messages</a></li>
                        <li><a class="dropdown-item" href="#">Settings</a></li>
                        <li><a class="dropdown-item" href="#">Dark mode</a></li>
                        <li><hr class="dropdown-divider"></li>
                        <li><a class="dropdown-item" href="../../../user/get/logout.php">Sign out</a></li>
                    </ul>
                </li>
            </ul>
            <form class="d-flex mt-3" action="" id="q">
                <input class="form-control me-2" type="search" name="" placeholder="Enter keyword" aria-label="Search" id="qry">
                <button class="btn btn-outline-dark" type="submit">Search</button>
            </form>
        </div>
    </div>
</nav>
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-3 sidebar">
            <h5 class="mt-3">What's new</h6>
            <div class="list-group list-group-flush">
                <a href="#" class="list-group-item list-group-item-action">
                    <div class="d-flex w-100 justify-content-between">
                        <h5 class="mb-1">Invitation</h5>
                        <small class="text-muted">3 days ago</small>
                    </div>
                    <p class="mb-1">Some placeholder content in a paragraph.</p>
                    <small class="text-muted">And some muted small print.</small>
                </a>
                <a href="#" class="list-group-item list-group-item-action" aria-current="true">
                    <div class="d-flex w-100 justify-content-between">
                        <h5 class="mb-1">Added</h5>
                        <small>3 days ago</small>
                    </div>
                    <p class="mb-1">Some placeholder content in a paragraph.</p>
                    <small>And some small print.</small>
                </a>
                <a href="#" class="list-group-item list-group-item-action">
                    <div class="d-flex w-100 justify-content-between">
                        <h5 class="mb-1">Removed</h5>
                        <small class="text-muted">3 days ago</small>
                    </div>
                    <p class="mb-1">Some placeholder content in a paragraph.</p>
                    <small class="text-muted">And some muted small print.</small>
                </a>
                <a href="#" class="list-group-item list-group-item-action">
                    <div class="d-flex w-100 justify-content-between">
                        <h5 class="mb-1">Transferred</h5>
                        <small class="text-muted">3 days ago</small>
                    </div>
                    <p class="mb-1">Some placeholder content in a paragraph.</p>
                    <small class="text-muted">And some muted small print.</small>
                </a>
            </div>            
        </div>
        <div class="col-sm-9 py-5">
            <div class="table-responsive mb-3">
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                        <tr>
                            <th scope="col"></th>
                            <th scope="col" style="width: 14.29%;">Mon</th>
                            <th scope="col" style="width: 14.29%;">Tue</th>
                            <th scope="col" style="width: 14.29%;">Wed</th>
                            <th scope="col" style="width: 14.29%;">Thu</th>
                            <th scope="col" style="width: 14.29%;">Fri</th>
                            <th scope="col" style="width: 14.29%;">Sat</th>
                        </tr>
                    </thead>
                    <tbody id="tableRec" style="vertical-align: middle;"></tbody>
                </table>
            </div>
        </div>
    </div>
</div>


<script>

function getSched() {

    var schedTable = new XMLHttpRequest;

    schedTable.open('GET', 'get/schedule_rec.php', 'true');
    
    schedTable.onload = function() {
        var sched = JSON.parse(this.responseText);
        console.log('sched: ', sched);

        showSched(sched);
    }
    
    schedTable.send();

}

getSched();

function showSched(arr) {

    tableRec.innerHTML = '';

    var schedDay = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
    var schedTime = ['8:00 am', '8:30 am', '9:00 am', '9:30 am', '10:00 am', '10:30 am', '11:00 am', '1:00 pm', '1:30 pm', '2:00 pm', '2:30 pm', '3:00 pm', '3:30 pm', '4:00 pm', '4:30 pm', '5:00 pm', '5:30 pm', '6:00 pm', '6:30 pm'];

    for (var t in schedTime) {

        var newRow = document.createElement('tr');
        var newHeader = document.createElement('th');
        var Mon = document.createElement('td');
        var Tue = document.createElement('td');
        var Wed = document.createElement('td');
        var Thu = document.createElement('td');
        var Fri = document.createElement('td');
        var Sat = document.createElement('td');

        newHeader.setAttribute('scope', 'row');
        newHeader.className = 'text-center';
        Mon.className = 'text-center';
        Mon.dataset.sched = 'Mon' + schedTime[t].replace(':', '');
        Tue.className = 'text-center';
        Tue.dataset.sched = 'Tue' + schedTime[t].replace(':', '');
        Wed.className = 'text-center';
        Wed.dataset.sched = 'Wed' + schedTime[t].replace(':', '');
        Thu.className = 'text-center';
        Thu.dataset.sched = 'Thu' + schedTime[t].replace(':', '');
        Fri.className = 'text-center';
        Fri.dataset.sched = 'Fri' + schedTime[t].replace(':', '');
        Sat.className = 'text-center';
        Sat.dataset.sched = 'Sat' + schedTime[t].replace(':', '');
        newHeader.innerHTML = schedTime[t];

        newRow.appendChild(newHeader);
        newRow.appendChild(Mon);
        newRow.appendChild(Tue);
        newRow.appendChild(Wed);
        newRow.appendChild(Thu);
        newRow.appendChild(Fri);
        newRow.appendChild(Sat);

        tableRec.appendChild(newRow);

    }
    
    for (var x in arr) {

        for (t in schedTime) {

            if (arr[x]['sched_time'] == schedTime[t]) {

                for (var d in schedDay) {

                    if (arr[x]['sched_day'] == schedDay[d]) {

                        var arr_time = arr[x]['sched_time'].replace(':', '');

                        var getTd = document.querySelector('td[data-sched=' + CSS.escape(schedDay[d]) + CSS.escape(arr_time) + ']');

                        getTd.innerHTML = '<b>' + arr[x]['subject_code'] + '</b>' + '<br>' + arr[x]['subject_title'];
                    }

                }
            }
        }
    }
}

</script>

</body>
</html>