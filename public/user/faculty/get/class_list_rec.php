<?php

    $method = $_SERVER['REQUEST_METHOD'];

    if(!isset($_SESSION)) { 
        session_start(); 
    }

    $search_id = $_SESSION['usr_handle'];

    $search_by_section = $_GET['subject_id'] ?? null;
    $sort_subject = $_GET['sort_subject'] ?? null;
    $unique_section = $_GET['unique_section'] ?? null;

    $all_class = [];

    if ($method === 'GET' && $search_by_section) {

        require '../../../../database.php';
        
        $get_section = $pdo->prepare('SELECT * FROM grades WHERE subject_term = :subject_id');
        $get_section->bindValue(':subject_id', $search_by_section);
    
        $get_section->execute();
        $section_fetch = $get_section->fetchAll(PDO::FETCH_ASSOC);
        
        if (!empty($section_fetch)) {

            foreach($section_fetch as $section) {

                $get_student = $pdo->prepare('SELECT * FROM students WHERE id = :student_id');
                $get_student->bindValue(':student_id', $section['student_id']);
            
                $get_student->execute();
                $student_fetch = $get_student->fetch(PDO::FETCH_ASSOC);

                $section['student_num'] = $student_fetch['student_num'];
                $section['first_name'] = $student_fetch['first_name'];
                $section['last_name'] = $student_fetch['last_name'];
                $section['course'] = $student_fetch['course'];
                $section['student_year'] = $student_fetch['student_year'];
                array_push($all_class, array($section));
            }
        }

        $class_json_arr = json_encode($all_class, JSON_PRETTY_PRINT);

        echo $class_json_arr;

    }

    else if ($method === 'GET' && $sort_subject == 'true') {

        require '../../../../database.php';

        get_subject($search_id, $pdo);

    }

    else if ($method === 'GET' && $unique_section == 'true') {

        require '../../../../database.php';

        get_subject($search_id, $pdo);

    }

    else {

        require '../../../database.php';

        $all_class = [];

        $get_subjects = $pdo->prepare('SELECT * FROM subject_term WHERE lecturer_id = :lecturer_id');
        $get_subjects->bindValue(':lecturer_id', $search_id);
    
        $get_subjects->execute();
        $subject_fetch = $get_subjects->fetchAll(PDO::FETCH_ASSOC);

        foreach ($subject_fetch as $subject) {

            $get_section = $pdo->prepare('SELECT * FROM grades WHERE subject_term = :subject_id');
            $get_section->bindValue(':subject_id', $subject['id']);
        
            $get_section->execute();
            $section_fetch = $get_section->fetchAll(PDO::FETCH_ASSOC);

            if (!empty($section_fetch)) {
                
                foreach($section_fetch as $section) {
    
                    $get_student = $pdo->prepare('SELECT * FROM students WHERE id = :student_id');
                    $get_student->bindValue(':student_id', $section['student_id']);
                
                    $get_student->execute();
                    $student_row = $get_student->fetch(PDO::FETCH_ASSOC);
    
                    $section['student_num'] = $student_row['student_num'];
                    $section['first_name'] = $student_row['first_name'];
                    $section['last_name'] = $student_row['last_name'];
                    $section['course'] = $student_row['course'];
                    $section['student_year'] = $student_row['student_year'];
                    array_push($all_class, array($section));
                }
            }

        }
    
        $class_json_arr = json_encode($all_class, JSON_PRETTY_PRINT);

    }

    function get_subject($id, $pdo) {
        $get_subject_row = $pdo->prepare('SELECT * FROM subject_term WHERE lecturer_id = :lecturer_id');
        $get_subject_row->bindValue(':lecturer_id', $id);
    
        $get_subject_row->execute();
        $subject_row_fetch = $get_subject_row->fetchAll(PDO::FETCH_ASSOC);

        $subject_json_arr = json_encode($subject_row_fetch, JSON_PRETTY_PRINT);

        echo $subject_json_arr;
    }
?>