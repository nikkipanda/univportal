<?php

    require_once '../../../../database.php';

    session_start();

    $search_id = $_SESSION['usr_handle'];

    $schedule_get = $pdo->prepare('SELECT * FROM subject_term WHERE lecturer_id = :lecturer_id');
    $schedule_get->bindValue(':lecturer_id', $search_id);

    $schedule_get->execute();
    $schedule_fetch = $schedule_get->fetchAll(PDO::FETCH_ASSOC);

    $schedule_json_arr = json_encode($schedule_fetch, JSON_PRETTY_PRINT);

    echo $schedule_json_arr;
?>