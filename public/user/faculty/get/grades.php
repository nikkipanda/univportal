<?php

    require_once '../../../../database.php';

    $method = $_SERVER['REQUEST_METHOD'];

    if(!isset($_SESSION)) { 
        session_start(); 
    }

    $raw_input = file_get_contents('php://input');
    $obj_grades = json_decode($raw_input);

    $grade_table_id = $obj_grades->id ?? null;
    $attendance = $obj_grades->attendance ?? null;
    $recitation = $obj_grades->recitation ?? null;
    $quiz = $obj_grades->quiz ?? null;
    $homework = $obj_grades->homework ?? null;
    $seatwork = $obj_grades->seatwork ?? null;
    $prelim = $obj_grades->prelim ?? null;
    $midterm = $obj_grades->midterm ?? null;
    $final = $obj_grades->final ?? null;

    $quiz_total = 0;
    $sw_total = 0;
    $hw_total = 0;
    $attendance_total = 0;
    $recitation_total = 0;
    $prelim_total = 0;
    $midterm_total = 0;
    $prelim_total = 0;
    $total_grade = 0;
    $final_grade = 0;

    if ($method === 'POST' && !empty($obj_grades)) {

        echo '<pre>dumpppp';
        var_dump($obj_grades);
        echo '</pre>';

        $get_sum = [];

        if (!empty($quiz)) {
            foreach($quiz as $q) {
                $quiz_total += $q;
            }
            $quiz_total = ave_grade($quiz_total, count($quiz), .2);
            array_push($get_sum, $quiz_total);
        }

        if (!empty($homework)) {
            foreach($homework as $h) {
                $hw_total += $h;
            }
            $hw_total = ave_grade($hw_total, count($homework), .1);
            array_push($get_sum, $hw_total);
        }

        if (!empty($seatwork)) {
            foreach($seatwork as $s) {
                $sw_total += $s;
            }
            $sw_total = ave_grade($sw_total, count($seatwork), .1);
            array_push($get_sum, $sw_total);
        }

        if (!empty($attendance)) {
            $attendance_total = ave_grade($attendance, 1, .05);
            array_push($get_sum, $attendance_total);
        }

        if (!empty($recitation)) {
            $recitation_total = ave_grade($recitation, 1, .05);
            array_push($get_sum, $recitation_total);
        }

        if (!empty($prelim)) {
            $prelim_total = ave_grade($prelim, 1, .2);
            array_push($get_sum, $prelim_total);
        }

        if (!empty($midterm)) {
            $midterm_total = ave_grade($midterm, 1, .25);
            array_push($get_sum, $midterm_total);
        }

        if (!empty($final)) {
            $final_total = ave_grade($final, 1, .30);
            array_push($get_sum, $final_total);
        }

        $total_grade = array_sum($get_sum);

        if ($total_grade > 97) {
            $final_grade = 1;
        } else if ($final_grade > 95) {
            $final_grade = 1.25;
        } else if ($final_grade > 90) {
            $final_grade = 1.5;
        } else if ($final_grade > 87) {
            $final_grade = 2;
        } else if ($final_grade > 84) {
            $final_grade = 2.25;
        } else if ($final_grade > 80) {
            $final_grade = 2.5;
        } else if ($final_grade > 70) {
            $final_grade = 3;
        } else {
            $final_grade = 4;
        }

    }

function ave_grade($total, $count, $percent) {

    $ave_grade = round(($total / $count) * $percent, 2);

    return $ave_grade;

}
?>