<?php

    include_once '../../partials/headers/user/header.php';

?>

<?php

    session_start();

    if(!$_SESSION['usr_handle']) {
        header('Location: ../../user/index.php');
    }

?>

<nav class="navbar sticky-top navbar-expand-lg navbar-light" style="background-color: rgb(234, 251, 255);">
    <div class="container-fluid my-3">
        <a class="navbar-brand" href="lecturer.php">Brand logo/name</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#toggleNav" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="toggleNav">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                <li class="nav-item">
                    <a class="nav-link active" aria-current="page" href="lecturer.php">Dashboard</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="toggleAxn" role="button" data-bs-toggle="dropdown" aria-expanded="false">Action</a>
                    <ul class="dropdown-menu" aria-labelledby="toggleAxn">
                        <li><a class="dropdown-item" href="faculty/schedule.php">Schedule</a></li>
                        <li><a class="dropdown-item" href="faculty/class_list.php">Class List</a></li>
                        <li><a class="dropdown-item" href="faculty/feedback.php">Feedback</a></li>
                        <li><hr class="dropdown-divider"></li>
                        <li><a class="dropdown-item" href="#">Dump</a></li>
                    </ul>
                </li>
                <li class="nav-item">
                    <a class="nav-link active" aria-current="page" href="#">F.A.Q.</a>
                </li>   
            </ul>
            <ul class="nav nav-pills">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" data-bs-toggle="dropdown" href="#" role="button" aria-expanded="false">Hi, <?php echo $_SESSION['usr_fname'] ?></a>
                    <ul class="dropdown-menu">
                        <li><a class="dropdown-item" href="#">Profile</a></li>
                        <li><a class="dropdown-item" href="#">Messages</a></li>
                        <li><a class="dropdown-item" href="#">Settings</a></li>
                        <li><a class="dropdown-item" href="#">Dark mode</a></li>
                        <li><hr class="dropdown-divider"></li>
                        <li><a class="dropdown-item" href="../../user/get/logout.php">Sign out</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-3 sidebar">
            <h5 class="mt-3">What's new</h6>
            <div class="list-group">
                <a href="#" class="list-group-item list-group-item-action">
                    <div class="d-flex w-100 justify-content-between">
                        <h5 class="mb-1">Invitation</h5>
                        <small class="text-muted">3 days ago</small>
                    </div>
                    <p class="mb-1">Some placeholder content in a paragraph.</p>
                    <small class="text-muted">And some muted small print.</small>
                </a>
                <a href="#" class="list-group-item list-group-item-action" aria-current="true">
                    <div class="d-flex w-100 justify-content-between">
                        <h5 class="mb-1">Added</h5>
                        <small>3 days ago</small>
                    </div>
                    <p class="mb-1">Some placeholder content in a paragraph.</p>
                    <small>And some small print.</small>
                </a>
                <a href="#" class="list-group-item list-group-item-action">
                    <div class="d-flex w-100 justify-content-between">
                        <h5 class="mb-1">Removed</h5>
                        <small class="text-muted">3 days ago</small>
                    </div>
                    <p class="mb-1">Some placeholder content in a paragraph.</p>
                    <small class="text-muted">And some muted small print.</small>
                </a>
                <a href="#" class="list-group-item list-group-item-action">
                    <div class="d-flex w-100 justify-content-between">
                        <h5 class="mb-1">Transferred</h5>
                        <small class="text-muted">3 days ago</small>
                    </div>
                    <p class="mb-1">Some placeholder content in a paragraph.</p>
                    <small class="text-muted">And some muted small print.</small>
                </a>
            </div>            
        </div>
        <div class="col-sm-9 py-5">
            <div class="container">
                <div class="row mb-4">
                    <a class="weatherwidget-io" href="https://forecast7.com/en/12d88121d77/philippines/" data-label_1="PHILIPPINES" data-label_2="WEATHER" data-font="Open Sans" data-icons="Climacons Animated" data-theme="pure" >PHILIPPINES WEATHER</a>
                </div>
                <div class="row mb-3">
                    <div class="col-sm-6">
                        <h5 class="m-2">Term</h5>
                        <div id="term"></div>
                    </div>
                    <div class="col-sm-6">
                        <h5 class="m-2">Class Lists</h5>
                        <div class="row mb-2">
                            <div class="col-md-7 mt-2" id="classDesc"></div>
                            <div class="col-md-5" id="classBtn"></div>
                        </div>
                        <div id="classList"></div>
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col-sm-6">
                        <h5 class="m-2">Subjects</h5>
                        <div id="subjects"></div>
                    </div>
                    <div class="col-sm-6">
                        <h5 class="m-2">Grades</h5>
                        <div id="grades"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script>
!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src='https://weatherwidget.io/js/widget.min.js';fjs.parentNode.insertBefore(js,fjs);}}(document,'script','weatherwidget-io-js');

var classList = document.getElementById('classList');
var classDesc = document.getElementById('classDesc');
var classBtn = document.getElementById('classBtn');
var term = document.getElementById('term');
var subjects = document.getElementById('subjects');
var grades = document.getElementById('grades');

uniqueSection();

term.innerHTML = 'Term 1 is currently ongoing';

function uniqueSection() {

    var getUnique = new XMLHttpRequest;

    getUnique.open('GET', 'faculty/get/class_list_rec.php?unique_section=true', true);

    getUnique.onload = function() {

        var unique = JSON.parse(this.responseText);
        var newClassList = document.createElement('p');
        var newClassUl = document.createElement('ul');
        var newCLassLink = document.createElement('a');

        newClassUl.className = 'list-group';
        newCLassLink.className = 'btn btn-primary';

        newClassList.innerHTML = 'You are currently handling ' + unique.length + ' classes.';
        newCLassLink.innerHTML = 'View class list ->';

        classList.appendChild(newClassList);

        for (var i in unique) {

            var newClassLi = document.createElement('li');
            var newClassSpan = document.createElement('span');

            newClassLi.className = 'list-group-item d-flex justify-content-between align-items-center';
            newClassSpan.className = 'badge bg-primary rounded-pill';

            newClassSpan.innerHTML = 'class length';
            newClassLi.innerHTML = 'Section ' + unique[i]['section'];
            
            newClassLi.appendChild(newClassSpan);

            newClassUl.appendChild(newClassLi);

        }

        classDesc.appendChild(newClassList);
        classBtn.appendChild(newCLassLink);
        classList.appendChild(newClassUl);
        
    }

    getUnique.send();

}

</script>

</body>
</html>