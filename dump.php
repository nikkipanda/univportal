<td>
    <a href="updateRec.php/?studentID=<?php echo $info['studentID'] ?>" type="button btn-block" class="btn btn-warning m-1">Update</a>
    <form action="deleteRec.php" method="POST" class="m-1">
        <input type="hidden" name="studentID" value="<?php echo $info['studentID'] ?>">
        <button type="submit" class="btn btn-danger">Delete</button>
    </form>
</td>

<a href="updateRec.php/?studentID=<?php echo $info['studentID'] ?>" type="button btn-block" class="btn btn-warning m-1">Update</a>
<a href="student/get.php?studentID=<?php echo $info['studentID']; ?>" type="button" class="btn btn-warning m-1" data-bs-toggle="modal" data-bs-target="#updateRec">Update</a>

---

<div id="tableRec">
        <table class="table" id="tableRec">
            <thead>
                <tr>
                    <th scope="col">Student Number</th>
                    <th scope="col">Last Name</th>
                    <th scope="col">First Name</th>
                    <th scope="col">Birthdate</th>
                    <th scope="col">Address</th>
                    <th scope="col">Course</th>
                    <th scope="col">Student Yr</th>
                    <th scope="col">Term</th>
                    <th scope="col">Mobile &#x23;</th>
                    <th scope="col">Email Address</th>
                    <th scope="col">Action</th>
                </tr>
            </thead>
            <tbody>
            <?php

                foreach ($studentFetch as $student => $info):

                    // echo '<pre> $info: ';
                    // var_dump($info['studentID']);
                    // echo '</pre>';

            ?>
                <tr>
                    <td><?php echo $info['studentID'] ?></td>
                    <td><?php echo $info['lastName'] ?></td>
                    <td><?php echo $info['firstName'] ?></td>
                    <td><?php echo $info['birthdate'] ?></td>
                    <td><?php echo $info['homeAddress'] ?></td>
                    <td><?php echo $info['course'] ?></td>
                    <td><?php echo $info['studentYear'] ?></td>
                    <td><?php echo $info['schoolTerm'] ?></td>
                    <td><?php echo $info['mobileNum'] ?></td>
                    <td><?php echo $info['emailAdd'] ?></td>
                    <td>
                    <button type="button" class="btn btn-warning m-1" data-bs-toggle="modal" data-bs-target="#updateRec" data-student-info="<?php echo $info; ?>" onclick="testJS()">Update</button>
                    <button type="button" class="btn btn-danger m-1" data-bs-toggle="modal" data-bs-target="#deleteRec" data-student-info="<?php echo $info['studentID'] ?>" onclick="testJS()">Delete</button>   
                    </td>
                </tr>
            <?php

                    endforeach;

                ?>
            </tbody>
        </table>

        <div class="input-group mb-3">
            <span class="input-group-text"><label for="birthdate">Date of birth</label></span>
            <input type="date" class="form-control" id="birthdate" name="bdate" value="<?php echo $bdate ?? $confDel['birthdate']; ?>" <?php if ($confDel['birthdate']): echo 'readonly'; endif; ?>>
        </div>

        ---

        <form action="" method="POST" enctype="multipart/form-data">
    <div class="container p-3" style="width: 500px;">
        <div class="input-group mb-3">
            <span class="input-group-text"><label for="firstname">First name</label></span>
            <input type="text" class="form-control" pattern="[a-zA-Z\s]+" name="fname" value="<?php echo $fname ?? $confDel['firstName']; ?>" <?php if ($confDel['firstName']): echo 'readonly'; endif; ?>>
        </div>
        <div class="input-group mb-3">
            <span class="input-group-text"><label for="lastname">Last name</label></span>
            <input type="text" class="form-control" pattern="[a-zA-Z\s]+" name="lname" value="<?php echo $lname ?? $confDel['lastName']; ?>" <?php if ($confDel['lastName']): echo 'readonly'; endif; ?>>
        </div>
        <?php if (!$getID): ?>
            <!-- for new record -->
            <div class="input-group mb-3">
                <span class="input-group-text">Gender</span>
                <div class="form-control" style="display: flex; align-items: center; flex-flow: row nowrap; justify-content: center;">
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="gender" id="male" value="m">
                        <label class="form-check-label" for="male">Male</label>
                    </div>
                </div>
                <div class="form-control" style="display: flex; align-items: center; flex-flow: row nowrap; justify-content: center;">
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="gender" id="female" value="f">
                        <label class="form-check-label" for="female">Female</label>
                    </div>
                </div>
            </div>
            
        <?php endif; ?>
        <?php if ($getID): ?>

            <div class="input-group mb-3">
                <span class="input-group-text"><label for="studentnumber">Student number</label></span>
                <input type="text" class="form-control" id="studentnumber" pattern="[\d]+" name="studnumber" value="<?php echo $getID ?>" readonly>
            </div>

        <?php elseif (!$getID): ?>
            <!-- for new record -->
            <div class="input-group mb-3">
                <span class="input-group-text"><label for="studentnumber">Student number</label></span>
                <input type="text" class="form-control" id="studentnumber" pattern="[\d]+" name="studnumber" value="<?php echo $studnumber ?>">
            </div>
        <?php endif; ?>

        <div class="input-group mb-3">
            <label class="input-group-text" for="course">Course</label>
            <select class="form-select" id="course" name="course" required <?php if ($confDel['course']): echo 'disabled'; endif; ?>>
                <option selected disabled>Select option</option>
                <option value="BSIT" <?php if ($confDel['course'] === 'BSIT'): echo 'selected'; endif; ?>>BSIT</option>
                <option value="BSCS" <?php if ($confDel['course'] === 'BSCS'): echo 'selected'; endif; ?>>BSCS</option>
                <option value="BSCE" <?php if ($confDel['course'] === 'BSCE'): echo 'selected'; endif; ?>>BSCE</option>
                <option value="BSBA" <?php if ($confDel['course'] === 'BSBA'): echo 'selected'; endif; ?>>BSBA</option>
            </select>
        </div>
        <div class="input-group mb-3">
            <label class="input-group-text" for="studentyear">Student year</label>
            <select class="form-select" id="studentyear" name="studyear" required <?php if ($confDel['studentYear']): echo 'disabled'; endif; ?>>
                <option selected disabled>Select option</option>
                <option value="Freshman" <?php if ($confDel['studentYear'] === 'Freshman'): echo 'selected'; endif; ?>>Freshman</option>
                <option value="Sophomore" <?php if ($confDel['studentYear'] === 'Sophomore'): echo 'selected'; endif; ?>>Sophomore</option>
                <option value="Junior" <?php if ($confDel['studentYear'] === 'Junior'): echo 'selected'; endif; ?>>Junior</option>
            </select>
            <label class="input-group-text" for="semester">Semester</label>
            <select class="form-select" id="semester" name="sem" required <?php if ($confDel['schoolTerm']): echo 'disabled'; endif; ?>>
                <option selected disabled>Select option</option>
                <option value="1" <?php if ($confDel['schoolTerm'] === '1'): echo 'selected'; endif; ?>>1</option>
                <option value="2" <?php if ($confDel['schoolTerm'] === '2'): echo 'selected'; endif; ?>>2</option>
                <option value="3" <?php if ($confDel['schoolTerm'] === '3'): echo 'selected'; endif; ?>>3</option>
            </select>
        </div>
        <div class="input-group mb-3">
            <span class="input-group-text"><label for="address">Home address</label></span>
            <textarea class="form-control" id="address" name="address"  <?php if ($confDel['homeAddress']): echo 'readonly'; endif; ?>><?php echo $address ?? $confDel['homeAddress']; ?></textarea>
        </div>
        <div class="input-group mb-3">
            <span class="input-group-text"><label for="mobilenum">Mobile number</label></span>
            <input type="text" class="form-control" pattern="[\d]{11}" id="mobilenum" name="mobile" value="<?php echo $mobile ?? $confDel['mobileNum']; ?>" <?php if ($confDel['mobileNum']): echo 'readonly'; endif; ?>>
        </div>
        <div class="input-group mb-3">
            <span class="input-group-text"><label for="emailaddress">Email address</label></span>
            <input type="email" class="form-control" id="emailaddress" name="email" value="<?php echo $email ?? $confDel['emailAdd']; ?>" <?php if ($confDel['emailAdd']): echo 'readonly'; endif; ?>>
        </div>
        <hr class="hrdivider">
        <?php if (!$getID): ?>
            <div class="input-group mb-3">
            <button type="submit" class="btn btn-primary btn-md mx-auto">Submit</button>
            </div>
        <?php endif; ?>
    </div>
</form>



----


// update Rec:

<?php

require_once '../database.php';
include_once 'student/get.php';

echo '<pre> $myJSON: ';
var_dump($studentInfo);
echo '</pre>';

// $getID = $_GET['studentID'];
$method = $_SERVER['REQUEST_METHOD'];

echo '<pre> getID: ';
var_dump($getID);
echo '</pre>';
// exit;

$warningMsg = [];
// $confDel = [
//   'studentID' => $getID,
//   'firstName' => '',
//   'lastName' => '',
//   'birthdate' => '',
//   'homeAddress' => '',
//   'course' => '',
//   'studentYear' => '',
//   'schoolTerm' => '',
//   'mobileNum' => '',
//   'emailAdd' => '',
//   'sex' => '',
// ];
$studnumber = $updateStudentInfo['studentID']; // ID on the record
$fname = $updateStudentInfo['firstName'];
$lname = $updateStudentInfo['lastName'];
$bdate = $updateStudentInfo['birthdate'];
$course = $updateStudentInfo['course'];
$studyear = $updateStudentInfo['studentYear'];
$sem = $updateStudentInfo['schoolTerm'];
$address = $updateStudentInfo['homeAddress'];
$mobile = $updateStudentInfo['mobileNum'];
$email = $updateStudentInfo['emailAdd'];

if ($method === 'POST'):
      
    require_once '../validate.php';

    if (empty($warningMsg)):

      echo 'NO WARNING'.'<br>';
      
      $statement = $pdo->prepare
      ("UPDATE students SET
          lastName = :lastName,
          firstName = :firstName,
          birthdate = :birthdate,
          homeAddress = :homeAddress,
          course = :course,
          studentYear = :studentYear,
          schoolTerm = :schoolTerm,
          mobileNum = :mobileNum,
          emailAdd = :emailAdd

        WHERE
          studentID = $getID
      ");

      $statement->bindValue(':lastName', $lname);
      $statement->bindValue(':firstName', $fname);
      $statement->bindValue(':birthdate', $bdate);
      $statement->bindValue(':homeAddress', $address);
      $statement->bindValue(':course', $course);
      $statement->bindValue(':studentYear', $studyear);
      $statement->bindValue(':schoolTerm', $sem);
      $statement->bindValue(':mobileNum', $mobile);
      $statement->bindValue(':emailAdd', $email);

      $statement->execute();

      header('Location: ../index.php');
      exit;

  endif;
endif;
?>

</body>
</html>


---------------------

<?php

    foreach ($warningMsg as $warning): 

?>

<div class="alert alert-danger alert-dismissible fade show" role="alert">
    <?php
        echo $warning;
    ?>
    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
</div>

<?php

    endforeach;

?>