<?php

// dump for btstrp validation

$fname = $_POST['fname'] ?? null;
$lname = $_POST['lname'] ?? null;
$gender = $_POST['gender'] ?? null;
$studnumber = $_POST['studnumber'] ?? null;
$bdate = $_POST['bdate'] ?? null;
$course = $_POST['course'] ?? null;
$studyear = $_POST['studyear'] ?? null;
$sem = $_POST['sem'] ?? null;
// $student_section = $_POST['student_section'] ?? null;
$address = $_POST['address'] ?? null;
$mobile = $_POST['mobile'] ?? null;
$email = $_POST['email'] ?? null;


if (!$fname):
    array_push($warningMsg, 'First name required');
endif;

if (!$lname):
    array_push($warningMsg, 'Last name required');
endif;

if ($gender == null):
    array_push($warningMsg, 'Gender required');
endif;

if ($studnumber && !$postID): // creating new record, check for duplicate

    foreach ($studentFetch as $studentnum => $s):

        if (in_array($studnumber, $s, true)):
            array_push($warningMsg, 'Student number already exists');
        endif;

    endforeach;

elseif(!$studnumber):
    array_push($warningMsg, 'Student number required');
endif;

if (!$bdate):
    array_push($warningMsg, 'Date of birth required');
endif;

if ($course == null):
    array_push($warningMsg, 'Course required');
endif;

if ($studyear == null):
    array_push($warningMsg, 'Student year required');
endif;

if ($sem == null):
    array_push($warningMsg, 'Semester required');
endif;

// if ($student_section == null):
//     array_push($warningMsg, 'Section required');
// endif;

if (!$address):
    array_push($warningMsg, 'Home address required');
endif;

if (!$mobile):
    array_push($warningMsg, 'Mobile number required');
endif;

if (!$email):
    array_push($warningMsg, 'Email address required');
endif;

?>